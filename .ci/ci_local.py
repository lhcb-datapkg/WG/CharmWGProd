#!/usr/bin/env python
from itertools import chain
import subprocess
import os

from gitlab import Gitlab
from git import Repo

repo = Repo('.')
gitlab = Gitlab('https://gitlab.cern.ch', private_token=os.environ['GITLAB_API_TOKEN'])
gitlab.auth()
project = gitlab.projects.get('lhcb-datapkg/WG/CharmWGProd')

env = {}
for var in project.variables.list():
    env[var.key] = var.value
env['CI_PROJECT_DIR'] = '/repo'
env['CI_COMMIT_SHA'] = repo.head.object.hexsha
env['CI_PROJECT_PATH'] = project.attributes['path_with_namespace']
env['CI_COMMIT_REF_NAME'] = repo.active_branch.name

subprocess.call([
    'docker', 'run', '--privileged', '-v', '/cvmfs:/cvmfs:ro,cached', '-v', os.getcwd()+':/repo', '-w', '/repo', '--rm', '-it',
] + list(chain(*[['-e', k+'='+v] for k, v in env.items()])) + [
    'gitlab-registry.cern.ch/lhcb-docker/python-singularity:centos7-singularity',
    'bash'  # source .ci/setup.sh
])

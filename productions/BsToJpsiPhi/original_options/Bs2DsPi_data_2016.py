from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS, TupleToolGeometry, FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto
from Configurables import TupleToolTagging, TupleToolL0Data, TupleToolL0Calo

# Unit
SeqPhys = GaudiSequencer("SeqPhys")

# Trigger lines
l0_lines = [
	'L0PhysicsDecision'
	#,'L0MuonDecision'
	,'L0DiMuonDecision'
	,'L0MuonHighDecision'
	,'L0HadronDecision'
	#,'L0ElectronDecision'
	#,'L0PhotonDecision'
	#,'L0MuonNoSPDDecision'
	]

hlt1_lines = [
	#'Hlt1DiMuonHighMassDecision'
	#,'Hlt1DiMuonLowMassDecision'
	#,'Hlt1SingleMuonNoIPDecision'
	#,'Hlt1SingleMuonHighPTDecision'
	#,'Hlt1SingleElectronNoIPDecision'
	#,'Hlt1TrackAllL0Decision'
	#,'Hlt1TrackAllL0TightDecision'
	'Hlt1TrackMuonDecision'
	#,'Hlt1VertexDisplVertexDecision'
	#,'Hlt1GlobalDecision'
	#,'Hlt1TrackMuonNoSPDDecision'
	#,'Hlt2SingleMuonNoSPDDecision'
	,'Hlt1TrackMVADecision'
	,'Hlt1TwoTrackMVADecision'
	,'Hlt1B2PhiPhi_LTUNBDecision'
	]

hlt2_lines = [
	#'Hlt2DiElectronHighMassDecision'
	#,'Hlt2DiElectronBDecision'
	#,'Hlt2B2HHLTUnbiasedDecision'
	#,'Hlt2Topo2BodySimpleDecision'
	#,'Hlt2Topo3BodySimpleDecision'
	#,'Hlt2Topo4BodySimpleDecision'
	'Hlt2Topo2BodyBBDTDecision'
	,'Hlt2Topo3BodyBBDTDecision'
	,'Hlt2Topo4BodyBBDTDecision'
	#,'Hlt2TopoMu2BodyBBDTDecision'
	#,'Hlt2TopoMu3BodyBBDTDecision'
	#,'Hlt2TopoMu4BodyBBDTDecision'
	#,'Hlt2TopoE2BodyBBDTDecision'
	#,'Hlt2TopoE3BodyBBDTDecision'
	#,'Hlt2TopoE4BodyBBDTDecision'
	,'Hlt2Topo2BodyDecision'
	,'Hlt2Topo3BodyDecision'
	,'Hlt2Topo4BodyDecision'
	#,'Hlt2TopoMu2BodyDecision'
	#,'Hlt2TopoMu3BodyDecision'
	#,'Hlt2TopoMu4BodyDecision'
	,'Hlt2IncPhiSidebandsDecision'
	,'Hlt2PhiIncPhiDecision'
	,'Hlt2PhiBs2PhiPhiDecision'
	]

mtl = l0_lines + hlt1_lines + hlt2_lines


importOptions("$STDOPTS/PreloadUnits.opts")

#location="/Event/BhadronCompleteEvent/Phys/B02DPiD2HHHBeauty2CharmLine/Particles"
location="/Event/BhadronCompleteEvent/Phys/BetaSBs2DsPiDetachedLine/Particles"

# Fill the tuples (root files)
tuple = DecayTreeTuple("ntuple")
tuple.Inputs = [location]
#tuple.Decay = "[ B0-> ^(D- -> ^K- ^pi- ^K+) ^pi+]CC"
#tuple.Branches = {
#	"B"       : "[ B0->  (D- ->  K-  pi-  K+)  pi+]CC",
#	"Ds"      : "[ B0-> ^(D- ->  K-  pi-  K+)  pi+]CC",
#	"hminus"  : "[ B0->  (D- -> ^K-  pi-  K+)  pi+]CC",
#	"piminus" : "[ B0->  (D- ->  K- ^pi-  K+)  pi+]CC",
#	"hplus"   : "[ B0->  (D- ->  K-  pi- ^K+)  pi+]CC",
#	"piplus"  : "[ B0->  (D- ->  K-  pi-  K+) ^pi+]CC"
#	}
tuple.Decay = "[ B_s0-> ^(D_s- -> ^K- ^pi- ^K+) ^pi+]CC"
tuple.Branches = {
	"B"       : "[ B_s0->  (D_s- ->  K-  pi-  K+)  pi+]CC",
	"Ds"      : "[ B_s0-> ^(D_s- ->  K-  pi-  K+)  pi+]CC",
	"hminus"  : "[ B_s0->  (D_s- -> ^K-  pi-  K+)  pi+]CC",
	"piminus" : "[ B_s0->  (D_s- ->  K- ^pi-  K+)  pi+]CC",
	"hplus"   : "[ B_s0->  (D_s- ->  K-  pi- ^K+)  pi+]CC",
	"piplus"  : "[ B_s0->  (D_s- ->  K-  pi-  K+) ^pi+]CC"
	}
'''
'''

# TupleToolDecay
tuple.addTool(TupleToolDecay, name='Ds')
tuple.addTool(TupleToolDecay, name='B')

# Tool list
tl= [ 
	"TupleToolAngles", 
	"TupleToolKinematic",
	"TupleToolPid",
	"TupleToolTrackInfo",
	"TupleToolPrimaries",
	"TupleToolPropertime",
	"TupleToolEventInfo",
	"TupleToolRecoStats",
	"TupleToolGeometry",
	"TupleToolTISTOS"
	]
tuple.ToolList += tl

# Refit
tuple.ReFitPVs = True

#Geo
TupleToolGeometry            = TupleToolGeometry('TupleToolGeometry')
TupleToolGeometry.RefitPVs   = True
TupleToolGeometry.Verbose    = True
tuple.addTool(TupleToolGeometry)

# Loki
from Configurables import LoKi__Hybrid__TupleTool

LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
LoKi_B.Variables = {
	"LOKI_FDCHI2" : "BPVVDCHI2",
	"LOKI_FDS"    : "BPVDLS",
	"LOKI_DIRA"   : "BPVDIRA",
	"LOKI_DTF_CTAU"        : "DTF_CTAU( 0, True )",
	"LOKI_DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
	"LOKI_DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
	"LOKI_DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )",
	"LOKI_MASS_DsConstr"   : "DTF_FUN ( M , True , 'D_s-' )" ,
	"LOKI_DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )"
	}

tuple.B.addTool(LoKi_B)
tuple.B.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B"]

# RecoStats to filling SpdMult, etc
tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
tuple.TupleToolRecoStats.Verbose=True

# Refit with mass constraint
from Configurables import TupleToolDecayTreeFitter
PVFit = TupleToolDecayTreeFitter("PVFit")
PVFit.Verbose = True
PVFit.UpdateDaughters = True
PVFit.constrainToOriginVertex = True

PVFitDs = TupleToolDecayTreeFitter("PVFitDs")
PVFitDs.Verbose = True
PVFitDs.UpdateDaughters = True
PVFitDs.constrainToOriginVertex = True
#PVFitDs.Substitutions = { 
#"B0 -> ^(D- -> K- pi- K+) pi+" : "D_s-", 
#"B~0-> ^(D+ -> K+ pi+ K-) pi-" : "D_s+" 
#}
PVFitDs.daughtersToConstrain = ["D_s-"]

PVFitB = TupleToolDecayTreeFitter("PVFitB")
PVFitB.Verbose = True
PVFitB.UpdateDaughters = True
PVFitB.constrainToOriginVertex = True
#PVFitB.Substitutions = { 
#"Meson -> ^(D- -> K- pi- K+) pi+" : "D_s-", 
#"Meson -> ^(D+ -> K+ pi+ K-) pi-" : "D_s+", 
#"B0 -> (X- -> K- pi- K+) pi+" : "B_s0", 
#"B~0-> (X+ -> K+ pi+ K-) pi-" : "B_s~0" 
#}
PVFitB.daughtersToConstrain = ["D_s-","B_s0"]

MassFit = TupleToolDecayTreeFitter("MassFit")
MassFit.Verbose = True
MassFit.constrainToOriginVertex = False
#MassFit.Substitutions = { 
#"B0 -> ^(D- -> K- pi- K+) pi+" : "D_s-", 
#"B~0-> ^(D+ -> K+ pi+ K-) pi-" : "D_s+" 
#}
MassFit.daughtersToConstrain = ["D_s-"]

tuple.B.addTool(PVFit)
tuple.B.addTool(PVFitDs)
#tuple.B.addTool(PVFitB)
#tuple.B.addTool(MassFit)
tuple.B.ToolList += ["TupleToolDecayTreeFitter/PVFit"]
tuple.B.ToolList += ["TupleToolDecayTreeFitter/PVFitDs"]
#tuple.B.ToolList += ["TupleToolDecayTreeFitter/PVFitB"]
#tuple.B.ToolList += ["TupleToolDecayTreeFitter/MassFit"]

# Trigger 
TisTos  = TupleToolTISTOS("TisTos")
TisTos.Verbose=True
TisTos.TriggerList = mtl

# Change TOSFracMu for Ds, B
from Configurables import TriggerTisTos
tuple.B.addTool( TisTos )
tuple.B.ToolList+=[ "TupleToolTISTOS/TisTos" ]
tuple.B.TisTos.addTool(TriggerTisTos())
tuple.B.TisTos.TriggerTisTos.TOSFracMuon = 0.
tuple.B.TisTos.TriggerTisTos.TOSFracEcal = 0.
tuple.B.TisTos.TriggerTisTos.TOSFracHcal = 0.

# specific tagging tuple tool
PTagging = TupleToolTagging('PTagging')
PTagging.Verbose = True
PTagging.OutputLevel = FATAL

from Configurables import BTaggingTool
PTagTool = PTagging.addTool(BTaggingTool, name="MyBTaggingTool")

from FlavourTagging.Tunings import applyTuning as applyFTTuning
applyFTTuning(PTagTool, tuning_version="Summer2017Optimisation_v4_Run2")
PTagging.TaggingToolName = PTagTool.getFullName()

tuple.B.addTool(PTagging)
tuple.B.ToolList+=["TupleToolTagging/PTagging"]

## Force update of CondDB to use proper momentum scaling
## For the data 
from Configurables import CondDB
CondDB(LatestGlobalTagByDataType = '2016')

## primary vertex selection
from Configurables import CheckPV
checkpv = CheckPV()

from Configurables import TrackScaleState
scale = TrackScaleState('TrackScaleState')

## seqence member, '&', make sure include DsMM here
SeqPhys.Members += [checkpv, scale, tuple]

########################################################################
from Configurables import DaVinci

DaVinci().DataType = "2016"
DaVinci().EvtMax = 5000 		               # Number of events
#DaVinci().EvtMax = -1 		               # Number of events
DaVinci().InputType = 'DST'
DaVinci().PrintFreq = 1000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple

DaVinci().Lumi = True
DaVinci().Simulation = False 
DaVinci().UserAlgorithms = [SeqPhys]       # two trees, for reco and gene information

'''
from Gaudi.Configuration import *
from GaudiConf import IOHelper

IOHelper('ROOT').inputFiles([
#'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/validation/Collision18/BHADRONCOMPLETEEVENT.DST/00074333/0000/00074333_00000054_1.bhadroncompleteevent.dst',
'~/myeos/stripping_dst/2016_00069603_00000508_1.bhadroncompleteevent.dst'

  ], clear=True)
#FileCatalog().Catalogs = [ 'xmlcatalog_file:pool_xml_catalog.xml' ]
'''

# Helper file to define data type for davinci

from Configurables import DaVinci
DaVinci().RootInTES = '/Event/Leptonic/'
DaVinci().Simulation   = False
DaVinci().EvtMax = -1                       # Number of events
DaVinci().DataType = "2016"
DaVinci().InputType	  = 'MDST'
from Configurables import CondDB
CondDB.LatestGlobalTagsByDataType = '2016'
DaVinci().Lumi = True

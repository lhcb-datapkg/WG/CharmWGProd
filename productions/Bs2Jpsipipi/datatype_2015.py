# Helper file to define data type for davinci

from Configurables import DaVinci
DaVinci().RootInTES = '/Event/Leptonic/'
DaVinci().Simulation   = False
DaVinci().EvtMax = -1                       # Number of events
DaVinci().DataType = "2015"
DaVinci().InputType	  = 'MDST'
from Configurables import CondDB
CondDB.LatestGlobalTagsByDataType = '2015'
DaVinci().Lumi = True




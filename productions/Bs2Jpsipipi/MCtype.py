from Configurables import DaVinci
DaVinci().Simulation   = True
DaVinci().EvtMax = -1                       # Number of events
DaVinci().InputType	  = 'DST'
DaVinci().Lumi = False
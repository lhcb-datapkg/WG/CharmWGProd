from Gaudi.Configuration import *

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import  DaVinciInit, GetIntegratedLuminosity
DaVinciInit().OutputLevel = 6
MessageSvc().OutputLevel                  = 6
GetIntegratedLuminosity().OutputLevel       = INFO
ToolSvc().OutputLevel                     = 6
NTupleSvc().OutputLevel                   = 6


from Configurables import  DecayTreeTuple
from DecayTreeTuple.Configuration import *

Location = 'Phys/B2JpsiHHBs2JpsiKstarLine/Particles'
tuple = DecayTreeTuple( 'Bd2JpsiKstTree' )
tuple.Inputs = [ Location ]
tuple.TupleName = "mytree"
tuple.Decay = '[B_s~0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*(892)0-> ^K+ ^pi-)]CC'
tuple.Branches = {
    "Bd"		:  "[B_s~0 -> (J/psi(1S) -> mu+ mu-) (K*(892)0-> K+ pi-)]CC",
    "R"		:  "[B_s~0 -> (J/psi(1S) -> mu+ mu-) ^(K*(892)0-> K+ pi-)]CC",
    "H1"		:  "[B_s~0 -> (J/psi(1S) -> mu+ mu-) (K*(892)0-> ^K+ pi-)]CC",
    "H2"		:  "[B_s~0 -> (J/psi(1S) -> mu+ mu-) (K*(892)0-> K+ ^pi-)]CC",
    "J_psi_1S"	:  "[B_s~0 -> ^(J/psi(1S) -> mu+ mu-) (K*(892)0-> K+ pi-)]CC"
   }

#particles
from Configurables import TupleToolDecay
names = ['Bd','R','H1','H2','J_psi_1S']
for part in names:
    tuple.addTool(TupleToolDecay, name = part)

from Configurables import TupleToolAngles, TupleToolEventInfo, TupleToolKinematic, TupleToolPid, TupleToolPrimaries, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTagging, TupleToolPropertime, TupleToolGeometry
tuple.ToolList = [ "TupleToolAngles"     ,
                   "TupleToolEventInfo"  ,
			 "TupleToolKinematic"  ,
			 "TupleToolPid"        ,
			 "TupleToolPrimaries"  ,
			 "TupleToolTrackInfo"  ,
			 "TupleToolRecoStats"  ,
			 "TupleToolGeometry"   ,
			 "TupleToolPropertime" ]

#Geo
TupleToolGeometry            = TupleToolGeometry('TupleToolGeometry')
TupleToolGeometry.RefitPVs   = True
TupleToolGeometry.Verbose    = True
tuple.addTool(TupleToolGeometry)

#Refit
tuple.ReFitPVs = True

#Trigger
from Configurables import TupleToolTISTOS
triglist = [ "L0MuonDecision"                 ,
             "L0DiMuonDecision"               ,
		 "Hlt1DiMuonHighMassDecision"     ,
		 "Hlt1TrackMuonDecision"          ,
		 "Hlt1TrackMVADecision"           ,
		 "Hlt2DiMuonDetachedDecision"     ,
		 "Hlt2DiMuonDetachedJPsiDecision" ,
		 "Hlt2DiMuonJPsiDecision"         ]

tistos = TupleToolTISTOS('tistos')
tistos.VerboseL0   = True
tistos.VerboseHlt1 = True
tistos.VerboseHlt2 = True
tistos.TriggerList = triglist[:]
tuple.Bd.addTool(tistos)
tuple.Bd.ToolList+=[ "TupleToolTISTOS/tistos" ]
tuple.J_psi_1S.addTool(tistos)
tuple.J_psi_1S.ToolList+=[ "TupleToolTISTOS/tistos" ]

#Change  TOSFracMu for Jpsi 
from Configurables import TriggerTisTos
tuple.J_psi_1S.tistos.addTool(TriggerTisTos())
tuple.J_psi_1S.tistos.TriggerTisTos.TOSFracMuon = 0.
tuple.J_psi_1S.tistos.TriggerTisTos.TOSFracEcal = 0.
tuple.J_psi_1S.tistos.TriggerTisTos.TOSFracHcal = 0.

#Loki
from Configurables import LoKi__Hybrid__TupleTool
LoKi_B = LoKi__Hybrid__TupleTool("LoKi_B")
LoKi_B.Variables =  { "LOKI_DTF_CTAU"        : "DTF_CTAU( 0, True )"                ,
			    "LOKI_DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )"               ,
			    "LOKI_DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )"             ,
			    "LOKI_DTF_CTAUs"       : "DTF_CTAUSIGNIFICANCE( 0, True )"    ,
			    "LOKI_MASS_JpsiConstr" : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
			    "ETA"                  : "ETA"                                ,
			    "Y"                    : "Y"                                  }
tuple.Bd.addTool(LoKi_B)
tuple.Bd.ToolList+=[ "LoKi::Hybrid::TupleTool/LoKi_B" ]

#DTF
from Configurables import TupleToolDecayTreeFitter
PVFit = TupleToolDecayTreeFitter("PVFit")
PVFit.Verbose = True
PVFit.UpdateDaughters = True
PVFit.constrainToOriginVertex = True

PVFitJpsi = TupleToolDecayTreeFitter("PVFitJpsi")
PVFitJpsi.Verbose = True
PVFitJpsi.UpdateDaughters = True
PVFitJpsi.constrainToOriginVertex = True
PVFitJpsi.daughtersToConstrain = [ "J/psi(1S)" ]

PVFitB0 = TupleToolDecayTreeFitter("PVFitB0")
PVFitB0.Verbose = True
PVFitB0.UpdateDaughters = True
PVFitB0.constrainToOriginVertex = True
PVFitB0.Substitutions = { "B_s~0 -> (J/psi(1S) -> mu+ mu-) (K*(892)0-> K+ pi-)"  : "B0" ,
                          "B_s0 -> (J/psi(1S) -> mu+ mu-) (K*(892)~0-> K- pi+)" : "B~0" }
PVFitB0.daughtersToConstrain = [ "J/psi(1S)", "B0" ]

tuple.Bd.addTool(PVFit)
tuple.Bd.ToolList+=[ "TupleToolDecayTreeFitter/PVFit" ]
tuple.Bd.addTool(PVFitJpsi)
tuple.Bd.ToolList+=[ "TupleToolDecayTreeFitter/PVFitJpsi" ]
tuple.Bd.addTool(PVFitB0)
tuple.Bd.ToolList+=[ "TupleToolDecayTreeFitter/PVFitB0" ]

#check pv
from Configurables import CheckPV
checkpv = CheckPV()

from Configurables import TrackScaleState
smear = TrackScaleState('TrackScaleState')

from Configurables import DaVinci
DaVinci().UserAlgorithms = [ checkpv, smear, tuple ]




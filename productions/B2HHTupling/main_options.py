ConfigStream = {
   '2015' : 'FTAG'                ,
   '2016' : 'FTAG'                ,
   '2017' : 'Bhadron'             ,
   '2018' : 'BhadronCompleteEvent',
}

from Configurables import DaVinci
year = DaVinci().DataType
if year not in ConfigStream.keys():
    raise ValueError('Found an unsupported year: '+year)


stream = ConfigStream[year]
FTfromDST = None
if stream == 'Bhadron': 
   FTfromDST = True
elif stream in ['FTAG', 'BhadronCompleteEvent']: 
   FTfromDST = False
else: 
   raise ValueError('Found wrong a stream name: '+stream)

##########################################################
##########################################################
from Gaudi.Configuration import *
from Configurables import CondDB, TupleToolDecay, DecayTreeTuple, LoKi__Hybrid__TupleTool, TupleToolDecayTreeFitter


LoKiTool = LoKi__Hybrid__TupleTool('LoKiTool')
preseltuple = DecayTreeTuple("PreSelB2HH")
preseltuple.Decay = "B0 -> ^pi+ ^pi-"
preseltuple.Inputs = [ "/Event/"+stream+"/Phys/B2HHBDTLine/Particles" ]
preseltuple.ToolList +=  [ "LoKi::Hybrid::TupleTool/LoKiTool"
                           ,"TupleToolKinematic"
                           ,"TupleToolGeometry"
                           ,"TupleToolPid"
                           ,"TupleToolPrimaries"
                           ,"TupleToolPropertime"
                           ,"TupleToolTrackInfo"
                           ,"TupleToolEventInfo"
                           ,"TupleToolTISTOS"
                           ,"TupleToolRecoStats"
                           ,"TupleToolTagging"
                           ,"TupleToolMCTruth"
                           ,"MCTupleToolPrimaries"
                           ,"TupleToolMCBackgroundInfo"
                         ]
preseltuple.addTool(LoKiTool)
preseltuple.LoKiTool.Variables = {
    "RichPIDk" : "PPINFO(LHCb.ProtoParticle.RichDLLk,-1000)"
    ,"RichPIDp" : "PPINFO(LHCb.ProtoParticle.RichDLLp,-1000)"
    ,"DOCA" : "DOCAMAX"}
from Configurables import TupleToolGeometry, TupleToolRecoStats, TupleToolKinematic, TupleToolTISTOS, TupleToolTagging, TupleToolStripping
preseltuple.addTool(TupleToolGeometry())
preseltuple.TupleToolGeometry.Verbose = True
preseltuple.addTool(TupleToolRecoStats())
preseltuple.TupleToolRecoStats.Verbose = True
preseltuple.addTool(TupleToolKinematic())
preseltuple.TupleToolKinematic.Verbose = True

# Flavour Tagging
tt_tagging = preseltuple.addTool(TupleToolTagging())
tt_tagging.Verbose = True
tt_tagging.AddTagPartsInfo = False
if FTfromDST:
    tt_tagging.UseFTfromDST = True
    DaVinci().InputType = "MDST"
else:
    tt_tagging.UseFTfromDST = False
    DaVinci().InputType = "DST"
    from Configurables import BTaggingTool
    btagtool = tt_tagging.addTool(BTaggingTool, name = "MyBTaggingTool")
    from FlavourTagging.Tunings import applyTuning as applyFTTuning
    applyFTTuning(btagtool, tuning_version="Summer2017Optimisation_v4_Run2")
    tt_tagging.TaggingToolName = btagtool.getFullName()

# Trigger
preseltuple.addTool(TupleToolTISTOS())
preseltuple.TupleToolTISTOS.Verbose = True
preseltuple.TupleToolTISTOS.VerboseL0 = True
preseltuple.TupleToolTISTOS.VerboseHlt1 = True
preseltuple.TupleToolTISTOS.VerboseHlt2 = True
preseltuple.TupleToolTISTOS.TriggerList = [ 'L0DiMuonDecision',
                                            'L0ElectronDecision',
                                            'L0HadronDecision',
                                            'L0MuonDecision',
                                            'L0JetElDecision',
                                            'L0JetPhDecision',
                                            'L0MuonEWDecision',
                                            'L0PhotonDecision',
                                            ###########################
                                            'Hlt1TrackMVADecision',
                                            'Hlt1TrackMuonDecision',
                                            'Hlt1TwoTrackMVADecision',
                                            ###########################
                                            'Hlt2B2HH_B2HHDecision',
                                            'Hlt2B2HHDecision',
                                            'Hlt2Topo2BodyDecision']

preseltuple.Branches = {"B0" : "B0 -> pi+ pi-"}
preseltuple.addTool(TupleToolDecay("B0"))
preseltuple.B0.ToolList += ["TupleToolDecayTreeFitter/MPiPi"
                           ,"TupleToolDecayTreeFitter/MKK"
                           ,"TupleToolDecayTreeFitter/MKPi"
                           ,"TupleToolDecayTreeFitter/MPiK"
                           ,"TupleToolDecayTreeFitter/MPK"
                           ,"TupleToolDecayTreeFitter/MKP"
                           ,"TupleToolDecayTreeFitter/MPPi"
                           ,"TupleToolDecayTreeFitter/MPiP"]
dtfPiPi = TupleToolDecayTreeFitter("MPiPi"
                                ,Verbose = True
                                ,constrainToOriginVertex = True)
dtfKK = TupleToolDecayTreeFitter("MKK"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "K+"
                                                 ,"Beauty ->  X+  ^pi-" : "K-"})
dtfKPi = TupleToolDecayTreeFitter("MKPi"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  pi-" : "K+"})
dtfPiK = TupleToolDecayTreeFitter("MPiK"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty ->  pi+ ^pi-" : "K-"})
dtfPK = TupleToolDecayTreeFitter("MPK"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "p+"
                                                 ,"Beauty ->  X+  ^pi-" : "K-"})
dtfKP = TupleToolDecayTreeFitter("MKP"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "K+"
                                                 ,"Beauty ->  X+  ^pi-" : "p~-"})
dtfPPi = TupleToolDecayTreeFitter("MPPi"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "p+"
                                                 ,"Beauty ->  X+  ^pi-" : "pi-"})
dtfPiP = TupleToolDecayTreeFitter("MPiP"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "pi+"
                                                 ,"Beauty ->  X+  ^pi-" : "p~-"})
preseltuple.B0.addTool(dtfPiPi)
preseltuple.B0.addTool(dtfKK)
preseltuple.B0.addTool(dtfKPi)
preseltuple.B0.addTool(dtfPiK)
preseltuple.B0.addTool(dtfPK)
preseltuple.B0.addTool(dtfKP)
preseltuple.B0.addTool(dtfPPi)
preseltuple.B0.addTool(dtfPiP)

#from Configurables import TupleToolMCTruth
#preseltuple.addTool(TupleToolMCTruth())
#preseltuple.TupleToolMCTruth.ToolList += ["MCTupleToolHierarchy"
#                                         ,"MCTupleToolKinematic"
#                                         ]

preseltuple.TupleName = "PreSelB2HH"

from Configurables import GaudiSequencer
myseq = GaudiSequencer("MySeq")
myseq.Members += [ preseltuple ]

from Configurables import LoKi__HDRFilter as StripFilter

_StripFilter = StripFilter( 'StripFilter',
                            Code="HLT_PASS('StrippingB2HHBDTLineDecision')",
                            Location="/Event/Strip/Phys/DecReports" )

DaVinci().EventPreFilters = [ _StripFilter ]
DaVinci().Lumi = True
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
DaVinci().Simulation = False
DaVinci().TupleFile = "b2hh.root"
DaVinci().UserAlgorithms = [ myseq ]



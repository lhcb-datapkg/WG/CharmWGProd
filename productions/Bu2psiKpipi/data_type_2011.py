from Configurables import DaVinci
DaVinci().DataType = "2011"
DaVinci().Simulation = False

from Configurables import CondDB, CondDBAccessSvc
CondDB().LatestGlobalTagByDataType = DaVinci().DataType

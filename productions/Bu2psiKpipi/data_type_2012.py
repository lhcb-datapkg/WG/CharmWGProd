from Configurables import DaVinci
DaVinci().DataType = "2012"
DaVinci().Simulation = False

from Configurables import CondDB, CondDBAccessSvc
CondDB().LatestGlobalTagByDataType = DaVinci().DataType

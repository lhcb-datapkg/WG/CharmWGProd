from Configurables import DaVinci
DaVinci().DataType = "2018"
DaVinci().Simulation = False

from Configurables import CondDB, CondDBAccessSvc
CondDB().LatestGlobalTagByDataType = DaVinci().DataType

#*********************************************************************************************#
#*********************************************************************************************#
#			This is an option file for the B-> muepi/Bc->muepi                    #
#			Developed: Spring 2020						      #
#For description of LoKi functors see Phys/LoKiPhys/python/LoKiPhys/functions.py on gitlab    #


from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import CombineParticles

import sys

#*********************************************************************************************#
#*********************************************************************************************#
#                               general configurations                                        #

MC = True
prompt = False
nostrip = True		#if want to apply our combiner
killstrip = not nostrip #if need to apply another version of stripping
mode = 'OS'
year = '2016'
DST = True

tag = ''
if nostrip: 
   tag = "_owncombiner"
if killstrip:
   tag = "_restripped"

polarity = 'mu'
mass = '2500'
tau = '100'

stream = ''
if MC:
    stream = 'AllStreams'
else:
    stream = ''

line = ''
if prompt and not nostrip:
    line = 'StrippingBu2MajoMuLLLine' #detached/prompt and opposite sign
elif not nostrip and not prompt: 
    line = 'StrippingBu2MajoMuDDLine' #more detached and opposite sign
elif nostrip:
    line = ""

dtt_long = DecayTreeTuple('Bu2muN2epi_long')
dtt_down = DecayTreeTuple('Bu2muN2epi_down')

#*********************************************************************************************#
#*********************************************************************************************#
#                               configurations per year                                       #

if MC and year =='2016' :
    DaVinci().TupleFile = 'Bu2muepi_mc_'+polarity+'_'+line+mode+'_m'+mass+'_tau'+tau+'_'+year+tag+'.root'
    DaVinci().PrintFreq = 1000
    DaVinci().CondDBtag = 'sim-20161124-2-vc-mu100'
    DaVinci().DDDBtag = 'dddb-20150724'

DaVinci().DataType = year
DaVinci().Simulation = MC
DaVinci().getProp("Simulation")
DaVinci().Lumi =  not DaVinci().Simulation
DaVinci().EvtMax = -1

if DST:
    DaVinci().InputType = 'DST'
else:
    DaVinci().InputType = 'MDST'
    DaVinci().RootInTES = '/Event/{0}'.format(stream)


#*********************************************************************************************#
#*********************************************************************************************#
#                       	fill nmytuple function			                      #

def fillBuTuple(mytuple, toolList, triggerList):


    mytuple.ToolList = []
    

    if mode == "SS":
      mytuple.addBranches ({
           "eN"  :    '[(B+ -> (Lambda0 -> pi- ^e+) mu+), (B- -> (Lambda~0 -> pi+ ^e-) mu-)]'
          ,'MuB'  :    '[(B+ -> (Lambda0 -> pi- e+) ^mu+), (B- -> (Lambda~0 -> pi+ e-) ^mu-)]'
          ,'Pion'   :      '[(B+ -> (Lambda0 -> ^pi- e+) mu+), (B- -> (Lambda~0 -> ^pi+ e-) mu-)]'
          ,'N'  :    '[(B+ -> ^(Lambda0 -> pi- e+) mu+), (B- -> ^(Lambda~0 -> pi+ e-) mu-)]'
          ,'Bu'  :       '[(^(B+ -> (Lambda0 -> pi- e+) mu+)), (^(B- -> (Lambda~0 -> pi+ e-) mu-))]'
          }) 
    elif mode == "OS" :
       mytuple.addBranches ({
           'eN'  :   ' [(B+ -> (Lambda0 -> pi+ ^e-) mu+), (B- -> (Lambda~0 -> pi- ^e+) mu-)]'
          ,'MuB'  :  '[(B+ -> (Lambda0 -> pi+ e-) ^mu+), (B- -> (Lambda~0 -> pi- e+) ^mu-)]'
          ,'Pion'   :    '[(B+ -> (Lambda0 -> ^pi+ e-) mu+), (B- -> (Lambda~0 -> ^pi- e+) mu-)]'
          ,'N'  :  '[(B+ -> ^(Lambda0 -> pi+ e-) mu+), (B- -> ^(Lambda~0 -> pi- e+) mu-)]'
          ,'Bu'  :  '[^(B+ -> (Lambda0 -> pi+ e-) mu+), ^(B- -> (Lambda~0 -> pi- e+) mu-)]'
          })

    LoKi_All=mytuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All") #set up our own general tool
    LoKi_All.Variables = {
          'MINIPCHI2' : "MIPCHI2DV(PRIMARY)"  #minimum chi2 distance of a particle trajectory to the primary vertex
        , 'MINIP' : "MIPDV(PRIMARY)" #IP to the primary vertex												#think+ask Wouter
        , 'IPCHI2_OWNPV' : "BPVIPCHI2()" #Chi2-IP on the realted PV														#ask Wouter
        , 'IP_OWNPV' : "BPVIP()" #IP on the related PV													#difference
        }
    
   
    mytuple.ToolList = toolList
    
 #   mytuple.addTupleTool("TupleToolEventInfo") #adds info on the event and run number, magnet polarity, etc. see: https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolEventInfo

    if MC:

       
        MCTruth=mytuple.addTupleTool("TupleToolMCTruth") #adds TRUTH info for the mytuple
        #MCTruth.addTupleTool("MCTupleToolKinematic") #adds basic kinematics (here same as TRUTH)
        MCTruth.addTupleTool("MCTupleToolHierarchy") #adds info about mother-daughter relation see: https://gitlab.cern.ch/lhcb/Analysis/blob/50d5462576e3ab8ef0b30cde7ce35319cddfcbd9/Phys/DecayTreeTuple/src/MCTupleToolHierarchy.cpp ; Note: GD denotes previous generation; GD_GD - two genrations above -> see code for more details
        MCTruth.addTupleTool('MCTupleToolDecayType') # LHCb Event Types (https://twiki.cern.ch/twiki/bin/view/LHCb/EventTypes) see: https://gitlab.cern.ch/lhcb/Analysis/blob/50d5462576e3ab8ef0b30cde7ce35319cddfcbd9/Phys/DecayTreeTuple/src/MCTupleToolDecayType.h
        #MCTruth.addTupleTool('MCTupleToolReconstructed') # Is MC pratilce reconstructable?: https://gitlab.cern.ch/lhcb/Analysis/blob/2e6f415e400e12da7bc6076fc4cd052c5fb03452/Phys/DecayTreeTupleMC/src/MCTupleToolReconstructed.cpp
        MCTruth.addTupleTool("MCTupleToolAngles")# info on cos theta between particles (mother and current particle)
 
    tt_track_tool = mytuple.addTupleTool("TupleToolTrackInfo") #track info see: https://gitlab.cern.ch/lhcb/Analysis/blob/3cd6f412ef3928af8deaac8f6fc1842f924381d3/Phys/DecayTreeTupleReco/src/TupleToolTrackInfo.cpp
    tt_track_tool.Verbose = True

    tupletool_TISTOS = mytuple.Bu.addTupleTool("TupleToolTISTOS") #TISTOS trigger info (NEEDS a triggerlist) see: https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolTISTOS
    tupletool_TISTOS.TriggerList = triggerList
    tupletool_TISTOS.Verbose = True
    tupletool_TISTOS.VerboseL0 = True
    tupletool_TISTOS.VerboseHlt1 = True
    tupletool_TISTOS.VerboseHlt2 = True


    # stripTool = tuple.Bu.addTupleTool( "TupleToolStripping" )



    LoKi_MuB=mytuple.MuB.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuB") #our own tuple tool for the MuB
    LoKi_MuB.Variables = {
        'PIDmu' : "PIDmu"   #muon particle identification value, see:
      , 'ProbGhost' : "TRGHP" #ghost track probability
      , 'TRACK_CHI2' : "TRCHI2DOF" #track chi2
      , 'NNK' : "PPINFO(PROBNNK)" #PPINFO: stand for protoparticle info. 
      , 'NNpi' : "PPINFO(PROBNNpi)"#	see: https://twiki.cern.ch/twiki/bin/view/LHCb/PIDCalibPackage					#ProbNN is used with protoparticle always?
      , 'NNmu' : "PPINFO(PROBNNmu)"#
      , 'NNe' : "PPINFO(PROBNNe)"
      }

    LoKi_N=mytuple.N.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Neutrino")
    LoKi_N.Variables = {
      'DTF_CHI2iNDOF' : "DTF_CHI2NDOF(True)" # Vertex chi2 per degree of freedom after decay tree fitter fit 				#ask Wouter
      , 'TAU' : "BPVLTIME()" #proper lifetime of a particle
      , 'DIRA_BPV' : "BPVDIRA" #os theta between momentum of a particle abd the direction of flight from the best PV to the decay vertex
      , 'Mcorr' : "BPVCORRM"# Simple functor to evaluate the corrected mass with respect to particle flight direction                   #ask Wouter
      , 'ENDVERTEX_CHI2' : "VFASPF(VCHI2/VDOF)"# VFASPF = Allows to apply vertex functors to the particle's endVertex() 
      , 'FD_Z' : "BPVVDZ" #The functor computes the delta_z-distance from the end vertex of the particle and the related primary vertex: VertexZDistanceWithTheBestPV 
      , 'FD_CHI2' : "BPVVDCHI2" #chi-2 of the above distance
      , 'ENDVERTEX_X' : "VFASPF(VX)" #vertex X position
      , 'ENDVERTEX_Y' : "VFASPF(VY)"
      , 'ENDVERTEX_Z' : "VFASPF(VZ)"
      , 'BPV_X' : "BPV(VX)" #best PV X position
      , 'BPV_Y' : "BPV(VY)"
      , 'BPV_Z' : "BPV(VZ)"
      #, 'INMUON' : "switch(INMUON,1,0)" #in acceptance of the muon stations; switch doesn't do anything -> check the original true/false.                  
      , 'BPVLTSIGNCHI2' : "BPVLTSIGNCHI2()"# The functor evaluates the signed chi2-significance of the proper lifetime of the particle using ILifetimeFitter tool.    #research more
      , 'DOCA_PI_MuN' : "DOCA(1,2)"#
      }

    LoKi_Bu=mytuple.Bu.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bu")
    LoKi_Bu.Variables = {
      'DTF_CHI2NDOF' : "DTF_CHI2NDOF(True)"
      , 'TAU'      : "BPVLTIME()"
      , 'DIRA_BPV' : "BPVDIRA"
      , 'Mcorr' : "BPVCORRM"
      , 'ENDVERTEX_CHI2' : "VFASPF(VCHI2/VDOF)"
      , 'FD_Z' : "BPVVDZ"
      , 'FD_CHI2' : "BPVVDCHI2"
      , 'ENDVERTEX_X' : "VFASPF(VX)"
      , 'ENDVERTEX_Y' : "VFASPF(VY)"
      , 'ENDVERTEX_Z' : "VFASPF(VZ)"
      , 'BPV_X' : "BPV(VX)"
      , 'BPV_Y' : "BPV(VY)"
      , 'BPV_Z' : "BPV(VZ)"
      , 'BPVLTSIGNCHI2' : "BPVLTSIGNCHI2()"
      }

    LoKi_eN=mytuple.eN.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_eN")
    LoKi_eN.Variables = {
        'PIDe' : "PIDe"
      , 'ProbGhost' : "TRGHP"
      , 'TRACK_CHI2' : "TRCHI2DOF"
      , 'NNK' : "PPINFO(PROBNNK)"
      , 'NNpi' : "PPINFO(PROBNNpi)"
      , 'NNmu' : "PPINFO(PROBNNmu)"
      , 'NNe' : "PPINFO(PROBNNe)"
    }
    mytuple.Bu.addTupleTool('TupleToolDecayTreeFitter/ConsB')
    mytuple.Bu.ConsB.constrainToOriginVertex = True
    mytuple.Bu.ConsB.Verbose = True
    mytuple.Bu.ConsB.daughtersToConstrain = ['B+']
    mytuple.Bu.ConsB.UpdateDaughters = True
    
'''
    from Configurables import LoKi__Hybrid__DictOfFunctors
    from Configurables import LoKi__Hybrid__Dict2Tuple
    from Configurables import LoKi__Hybrid__DTFDict as DTFDict

    DictTuple = mytuple.Bu.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")

    # We need a DecayTreeFitter. DTFDict will provide the fitter and the connection to the tool chain
    # we add it as a source of data to the Dict2Tuple
    DictTuple.addTool(DTFDict,"DTF")
    DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
    DictTuple.NumVar = 14     # reserve a suitable size for the dictionaire

    # configure the DecayTreeFitter in the usual way
    DictTuple.DTF.constrainToOriginVertex = True
    DictTuple.DTF.daughtersToConstrain = ["B+"]

    # Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
    # these functors will be applied to the refitted(!) decay tree
    # they act as a source to the DTFDict
    DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"mydict")
    DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/mydict"

    DictTuple.DTF.mydict.Variables = {
      "DTFDict_Bu_PT"            : "PT",
      "DTFDict_Bu_M"             : "M",
      "DTFDict_Bu_PE"            : "E",
      "DTFDict_Bu_PX"            : "PX", 
      "DTFDict_Bu_PY"            : "PY",
      "DTFDict_Bu_PZ"            : "PZ", 
      "DTFDict_Bu_TAU"           : "BPVLTIME()",

      "DTFDict_N_PT"           : "CHILD(PT, '[B+ -> ^Lambda0 mu+]CC')",
      "DTFDict_N_M"            : "CHILD(M,'[B+ -> ^Lambda0 mu+]CC')",
      "DTFDict_N_PE"           : "CHILD(E,'[B+ -> ^Lambda0 mu+]CC')",
      "DTFDict_N_PX"           : "CHILD(PX,'[B+ -> ^Lambda0 mu+]CC')",
      "DTFDict_N_PY"           : "CHILD(PY,'[B+ -> ^Lambda0 mu+]CC')",
      "DTFDict_N_PZ"           : "CHILD(PZ,'[B+ -> ^Lambda0 mu+]CC')",
      "DTFDict_N_TAU"           : "CHILD(BPVLTIME(),'[B+ -> ^Lambda0 mu+]CC')"
      }
'''
'''

    LoKi_DTFFun = mytuple.Bu.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_DTFFun")
    LoKi_DTFFun.Variables = {
     "DTFFun_B_P_constrainedB"   : "DTF_FUN(P, True, 'B+')",
     "DTFFun_B_PT_constrainedB"   : "DTF_FUN(PT, True, 'B+')",
     "DTFFun_B_PX_constrainedB"   : "DTF_FUN(PX, True, 'B+')",
     "DTFFun_B_PY_constrainedB"   : "DTF_FUN(PY, True, 'B+')",
     "DTFFun_B_PZ_constrainedB"   : "DTF_FUN(PZ, True, 'B+')",
     "DTFFun_B_PE_constrainedB"   : "DTF_FUN(E, True, 'B+')",
     "DTFFun_B_M_constrainedB"    : "DTF_FUN(M, True, 'B+')",
 #    "DTFFun_B_TAU_constrainedB"  : "DTF_FUN(BPVLTIME(), True, 'B+')",


     "DTFFun_B_P_constrainedVertexB"   : "DTF_FUN(P, True, "")",
     "DTFFun_B_PT_constrainedVertexB"   : "DTF_FUN(PT, True, "")",
     "DTFFun_B_PX_constrainedVertexB"   : "DTF_FUN(PX, True, "")",
     "DTFFun_B_PY_constrainedVertexB"   : "DTF_FUN(PY, True, "")",
     "DTFFun_B_PZ_constrainedVertexB"   : "DTF_FUN(PZ, True, "")",
     "DTFFun_B_PE_constrainedVertexB"   : "DTF_FUN(E, True, "")",
     "DTFFun_B_M_constrainedVertexB"    : "DTF_FUN(M, True, "")",    
     "DTFFun_B_TAU_constrainedVertexB" : "DTF_FUN(BPVLTIME(), True, "")",

     "DTFFun_B_M_constrained_epi"       :  "DTF_FUN(M, True, strings(['e-','pi+']))",
     "DTFFun_B_PE_constrained_epi"       :  "DTF_FUN(E, True,  strings(['e-','pi+']))",
     "DTFFun_B_PX_constrained_epi"       :  "DTF_FUN(PX, True,  strings(['e-','pi+']))",
     "DTFFun_B_PY_constrained_epi"       :  "DTF_FUN(PY, True, strings(['e-','pi+']))",
     "DTFFun_B_PZ_constrained_epi"       :  "DTF_FUN(PZ, True,  strings(['e-','pi+']))",
     "DTFFun_B_PT_constrained_epi"       :  "DTF_FUN(PT, True,  strings(['e-','pi+']))",
     "DTFFun_B_P_constrained_epi"       :  "DTF_FUN(P, True,  strings(['e-','pi+']))",
#     "DTFFun_B_TAU_constrained_epi"     :  "DTF_FUN(BPVLTIME(), True, strings(['e-', 'pi+']))", 
      

     "DTFFun_DTF_CHI2_constrainedB"   : "DTF_CHI2(True, 'B+')",
     "DTFFun_DTF_NDOF_constrainedB"   : "DTF_NDOF(True, 'B+')",

     "DTFFun_DTF_CHI2_constrainedVertexB"   : "DTF_CHI2(True, "")",
     "DTFFun_DTF_NDOF_constrainedVertexB"   : "DTF_NDOF(True, "")",
 
     "DTFFun_DTF_CHI2_constrained_epi"   : "DTF_CHI2(True,  strings(['e-','pi+']))",
     "DTFFun_DTF_NDOF_constrained_epi"   : "DTF_NDOF(True,  strings(['e-','pi+']))",

     "DTFFun_N_M_constrainedB"       : "DTF_FUN(CHILD(M,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
     "DTFFun_N_PE_constrainedB"      : "DTF_FUN(CHILD(E,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
     "DTFFun_N_PX_constrainedB"      : "DTF_FUN(CHILD(PX, '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
     "DTFFun_N_PY_constrainedB"      : "DTF_FUN(CHILD(PY, '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
     "DTFFun_N_PZ_constrainedB"      : "DTF_FUN(CHILD(PZ, '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
     "DTFFun_N_PT_constrainedB"      : "DTF_FUN(CHILD(PT, '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
     "DTFFun_N_P_constrainedB"      : "DTF_FUN(CHILD(P, '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",
   #  "DTFFun_N_TAU_constrainedB"      : "DTF_FUN(CHILD(BPVLTIME(), '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, 'B+')",

     "DTFFun_N_M_constrainedVertexB"       : "DTF_FUN(CHILD(M,  '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_PE_constrainedVertexB"      : "DTF_FUN(CHILD(E,  '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_PX_constrainedVertexB"      : "DTF_FUN(CHILD(PX, '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_PY_constrainedVertexB"      : "DTF_FUN(CHILD(PY, '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_PZ_constrainedVertexB"      : "DTF_FUN(CHILD(PZ, '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_PT_constrainedVertexB"      : "DTF_FUN(CHILD(PT, '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_P_constrainedVertexB"      : "DTF_FUN(CHILD(P, '[B+ -> ^Lambda0 mu+]CC'), True, "")",
     "DTFFun_N_TAU_constrainedVertexB"      : "DTF_FUN(CHILD(BPVLTIME(), '[B+ -> ^Lambda0 mu+]CC'), True, "")",

     "DTFFun_N_M_constrained_epi"       : "DTF_FUN(CHILD(M,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
     "DTFFun_N_PE_constrained_epi"       : "DTF_FUN(CHILD(E,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
     "DTFFun_N_PT_constrained_epi"       : "DTF_FUN(CHILD(PT,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
     "DTFFun_N_PX_constrained_epi"       : "DTF_FUN(CHILD(PX,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
     "DTFFun_N_PY_constrained_epi"       : "DTF_FUN(CHILD(PY,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
     "DTFFun_N_PZ_constrained_epi"       : "DTF_FUN(CHILD(PZ,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
     "DTFFun_N_P_constrained_epi"       : "DTF_FUN(CHILD(P,  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))",
  #   "DTFFun_N_TAU_constrained_epi"       : "DTF_FUN(CHILD(BPVLTIME(),  '[(B+ -> ^Lambda0 mu+), (B- -> ^Lambda~0 mu-)]'), True, strings(['e-','pi+']))" 
    }

'''
#***************************************************************************************************************#
#***************************************************************************************************************#
#                                     LIST OF DEFAULT TOOLS                                                     #

tool_list = [
         "TupleToolKinematic"# adds general kinematics
       , "TupleToolMuonPid" #adds muon pid 
       , "TupleToolPid" #DLL and PID information of the particle
       , "TupleToolANNPID"#NeuralNet-based PID information of the particle
       , "TupleToolGeometry" #geometrical variables (IP, vertex position, etc) of the particle
       , "TupleToolEventInfo" #adds info on the event and run number, magnet polarity, etc. see: https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolEventInfo
       , "TupleToolRecoStats"#Reconstruction tracks : Num of charged/neutral protoparticles and muon/best tracjs
       , "TupleToolPrimaries"#info of the proimary vertices
]
tool_list2 = [
         "TupleToolKinematic"# adds general kinematics
       , "TupleToolMuonPid" #adds muon pid 
       , "TupleToolPid" #DLL and PID information of the particle
       , "TupleToolANNPID"#NeuralNet-based PID information of the particle
       , "TupleToolGeometry" #geometrical variables (IP, vertex position, etc) of the particle
       , "TupleToolEventInfo" #adds info on the event and run number, magnet polarity, etc. see: https://twiki.cern.ch/twiki/bin/view/LHCb/TupleToolEventInfo
       , "TupleToolRecoStats"#Reconstruction tracks : Num of charged/neutral protoparticles and muon/best tracjs
       , "TupleToolPrimaries"#info of the proimary vertices
]
#***************************************************************************************************************#
#***************************************************************************************************************#
#                                     MY OWN COMBINER                                                           #
if nostrip: 
    from PhysConf.Selections import Selection
    from PhysConf.Selections import CombineSelection, FilterSelection
    from PhysConf.Selections import SelectionSequence
    from StandardParticles import StdLoosePions, StdLooseMuons, StdLooseElectrons, StdNoPIDsDownPions, StdNoPIDsDownElectrons
    from PhysConf.Selections import ValidBPVSelection
    from Configurables import ParticleVertexFitter
   # Build the B -> L0 mu from the kaons

    #Add possible selection if needed
    _mu = FilterSelection(
       'Sel_mu',
       [StdLooseMuons],
       Code=""
    )

    decay_Lambda0 = ''
    if mode == 'SS':
        decay_Lambda0 = ['Lambda0 -> pi- e+', 'Lambda~0 -> pi+ e-']
    else:
        decay_Lambda0 = ['Lambda0 -> pi+ e-', 'Lambda~0 -> pi- e+']
     

    Lambda0 = CombineParticles(
      'Combine_Lambda0',
      DecayDescriptors = decay_Lambda0,
      ParticleCombiners = {"" : "ParticleVertexFitter" },
      DaughtersCuts   = { "e-" : "PT>300*MeV"
                       , "pi+" : "PT>300*MeV"
                       , "pi-" : "PT>300*MeV"
                       , "e+" : "PT>300*MeV"},
      CombinationCut= "(AM > 200*MeV) & (AM < 6000*MeV)", 
      MotherCut= "(VFASPF(VCHI2) < 20.0) & (M > 300*MeV) & (M < 5000*MeV)"
    )

    Lambda0_long_sel = Selection(
        'Sel_Lambda0_long',
        Algorithm=Lambda0,
        RequiredSelections=[StdLoosePions, StdLooseElectrons]   #Long container
    )

    #Lambda0_long_sel = ValidBPVSelection( Lambda0_long_sel )

    Lambda0_down_sel = Selection(
        'Sel_Lambda0_down',
        Algorithm=Lambda0,
        RequiredSelections=[StdNoPIDsDownPions, StdNoPIDsDownElectrons]   #Downstream containers
    )

    #Lambda0_down_sel = ValidBPVSelection(Lambda0_down_sel)

       #Combine L0 and muons into B
    B  = CombineParticles(
        'Combine_B',
        DecayDescriptor = "[B+ -> mu+ Lambda0]cc",
        ParticleCombiners = {"": "ParticleVertexFitter" }, 
	DaughtersCuts = { "mu-" : "PT>500*MeV"
			, "mu+" : "PT>500*MeV"},
        CombinationCut= "(AM > 1000*MeV) & (AM < 7000*MeV)",
        MotherCut= "(VFASPF(VCHI2) < 20.0) & (M > 4000*MeV) & (M < 6500*MeV)"
    )
    B_sel_long = Selection(
        'Sel_B_long',
        Algorithm=B,
        RequiredSelections=[Lambda0_long_sel, StdLooseMuons]
    )

    #B_sel_long = ValidBPVSelection(B_sel_long)

    B_sel_down = Selection(
        'Sel_B_down',
        Algorithm=B,
        RequiredSelections=[Lambda0_down_sel, StdLooseMuons]
    )

    #B_sel_down = ValidBPVSelection(B_sel_down)

    B_seq_long = SelectionSequence('B_Seq_long', TopSelection=B_sel_long)
    B_seq_down = SelectionSequence('B_Seq_down', TopSelection=B_sel_down)

    from Configurables import CheckPV, GaudiSequencer
    
    tupleseq_sel_down = GaudiSequencer('tupleseq_sel')
    tupleseq_sel_down.Members = [CheckPV(), B_seq_down.sequence()]
    tupleseq_sel_long = GaudiSequencer('tupleseq_sel2')
    tupleseq_sel_long.Members = [CheckPV(), B_seq_long.sequence()]
    
    DaVinci().UserAlgorithms += [tupleseq_sel_down, tupleseq_sel_long]

    dtt_long.Inputs = B_seq_long.outputLocations()
    dtt_down.Inputs = B_seq_down.outputLocations()




else:
    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive

    if killstrip: ###### to kill the stripping line if needed
        from Configurables import EventNodeKiller
        event_node_killer = EventNodeKiller('StripKiller')
        event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

    ################################################### here we start to stripp MC
    stripping='stripping28r1'
    config  = strippingConfiguration(stripping)
    archive = strippingArchive(stripping)
    streams = buildStreams(stripping=config, archive=archive) 

    # Select my line
    MyStream = StrippingStream("MyStream")
    MyLines = [ line ]

    for stream in streams: 
        for line in stream.lines:
            if line.name() in MyLines:
                MyStream.appendLines( [ line ] ) 

    # Configure Stripping
    from Configurables import ProcStatusCheck
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf( Streams = [ MyStream ],
                        MaxCandidates = 2000,
                        AcceptBadEvents = False,
                        BadEventSelection = filterBadEvents )

    dtt.Inputs = ['Phys/{0}/Particles'.format(line)]  #### replace

    
    if killstrip:
      DaVinci().appendToMainSequence( [ event_node_killer, sc.sequence() ] )
    else:
      DaVinci().appendToMainSequence(  sc.sequence() )
  

#***************************************************************************************************************#
#***************************************************************************************************************#
#                                     TRIGGER LINES      							#

TrigList  = []
L0_lines_run2= [
         "L0MuonDecision"
	,"L0ElectronDecision"                                   #for electron HLT1 lines
	,"L0CALODecision" 					#for allL0  hlt1 line
]
TrigList += L0_lines_run2
HLT1_lines_run2 = [
          "Hlt1TrackMuonDecision"				#PreFitP = 5.7 GeV, PreFitPT = 0.57 GeV, PT = 0.6 GeV, P = 6 GeV, IPChi2 = 7.4; TrChi2 = 3; TrGP = 999; muon decision as in the B->Kmue  analysis
	, "Hlt1TrackElectronDecision"				#P = 6GeV, PT = 0.5 GeV, IP = 0.1, IPChi2 = 9, TrChi2 = 4, L0ElectronThreshold = 254
	, "Hlt1SingleElectronNoIPDecision"  				#same as TrackElectron, but no IP cuts and P = 8GeV, PT = 4.8 GeV.
        , "Hlt1TrackAllL0Decision"				#NVeloHits = 9, VeloQcut = 3; NThits = 16; P = 6GeV; PT = 1.3GeV; IPChi2 = 13; TrChi2 = 2; TrGP = 999
        , "Hlt1TrackMVADecision" 				#PT= 1GeV - 25GeV, MinIPChi2 = 7.4; TrChi2 = 2.5; TrGP = 0.2, P1 =1, P2=1, P3=1.1;
        , "Hlt1TwoTrackMVADecision" 				#P = 5GeV; PT = 0.5GeV; TrGP = 999; TrChi2 = 2.5; IPChi2 = 4; MCor = 1-10^6 GeV; Eta = 2-5; MinDirA = 0; V0PT = 2GeV; VxChi2 = 10; Threshold = 0.96; MVS vars: VFASPF(VCHI2); BPVVDCHI2; SUMTREE(PT, ISBASIC); NINTREE(ISBASIC && (BPVIPCHI2()<16)) 
        , "Hlt1TrackMuonMVADecision"				#PT = 1GeV - 25GeV; MinIPChi2 = 7.4; TrChi2 = 2.5; TrGP = 0.2, P1 =1, P2=1, P3=1.1; #input Muons
        , "Hlt1SingleMuonHighPTDecision"			#P = 8GeV, PT=6GeV, TrChi2 = 4, TrGP = 999
	, "Hlt1SingleMuonNoIPDecision"					#P = 6GeV, PT = 1.3GeV, TrChi2 =4, TrGP=999, NTHits = 16, NVeloHits = 9, VeloQcut = 3, isMuon.
]
TrigList += HLT1_lines_run2
HLT2_lines_run2 =[
         'Hlt2SingleMuonDecision',				#IP = 0.25 mm; IPChi2 = 100;
         'Hlt2SingleMuonHighPTDecision',			#IP = 0.25 mm; IPChi2 = 100; PT = 10 GeV
         'Hlt2SingleMuonLowPTDecision',				#IP = 0.25 mm; IPChi2 = 100; PT = 4.8 GeV
         'Hlt2TopoMu2BodyDecision',				#
         'Hlt2TopoMu3BodyDecision',
	 'Hlt2Topo2BodyBBDTDecision', 
	 'Hlt2Topo3BodyBBDTDecision' 
]
TrigList += HLT2_lines_run2
trigger_list = TrigList

decay = ''
if mode == 'SS':#SS
    decay = "[(B+ -> ^(Lambda0 -> ^pi- ^e+) ^mu+), (B- -> ^(Lambda~0 -> ^pi+ ^e-) ^mu-)]"#detached
else:#OS
    decay = "[(B+ -> ^(Lambda0 -> ^pi+ ^e-) ^mu+), (B- -> ^(Lambda~0 -> ^pi- ^e+) ^mu-)]"#detached        

other_decay = "[B+ => ^(Lambda0 => ^pi+ ^e-) ^mu+]CC"

dtt_long.Decay = decay
dtt_down.Decay = decay

fillBuTuple(dtt_long, tool_list, trigger_list)
fillBuTuple(dtt_down, tool_list2, trigger_list)

from Configurables import CheckPV, GaudiSequencer
## using CheckPV tool to avoid crushing when there is no PV in event

tupleseq_down = GaudiSequencer('tupleseq')
tupleseq_down.Members = [CheckPV(), dtt_down]

tupleseq_long = GaudiSequencer('tupleseq2')
tupleseq_long.Members = [CheckPV(), dtt_long]

mc_basic_loki_vars = {
    'ETA': 'MCETA',
    'PHI': 'MCPHI',
    'PT': 'MCPT',
    'PX': 'MCPX',
    'PY': 'MCPY',
    'PZ': 'MCPZ',
    'E': 'MCE',
    'P': 'MCP',
}

DaVinci().UserAlgorithms +=[tupleseq_long, tupleseq_down]


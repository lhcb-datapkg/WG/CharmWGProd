from Configurables import DaVinci
dv = DaVinci()
# data_type = '2017'
data_type = DaVinci().DataType
# data or MC?
IsMC=DaVinci().Simulation
# index
Idx = data_type
if IsMC:
    Idx+='-MC'
    Idx='12105160-'+Idx
# Testing
files = {
    '2015': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/BHADRON.MDST/00068487/0001/00068487_00012312_1.bhadron.mdst"],
    '2016': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/BHADRON.MDST/00069593/0001/00069593_00014372_1.bhadron.mdst"],
    '2017': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/BHADRON.MDST/00071571/0001/00071571_00012352_1.bhadron.mdst"],
    '2018': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/BHADRON.MDST/00080044/0001/00080044_00012126_1.bhadron.mdst"],
    '12105160-2018-MC': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.MDST/00100352/0000/00100352_00000613_7.AllStreams.mdst"]
}
dv.Input = files[Idx]
dv.EvtMax=1000 # testing

# WG productions options for D0 → Ks H H

The options contained in this directory generate ntuples for:

1. DstarTag

* `D*(2010)+ -> (D0 -> H1+ H2- (KS0 -> pi+ pi-)) pi+`
* `B0 -> (D*(2010)+ -> (D0 -> H1+ H2- (KS0 -> pi+ pi-)) pi+) mu-`

2. SecondaryTag

* `B- -> (D0 -> H1+ H2- (KS0 -> pi+ pi-)) mu-`
* `B0 -> (D*(2010)+ -> (D0 -> H1+ H2- (KS0 -> pi+ pi-)) pi+) mu-`

Where `H1` and `H2` are any combination of kaons and pions.

## Generating the `info.json` file

As there are a lot of sub-productions in this WG production, the `info.json` file is generated using a script.
Examples of valid arguments:

```bash
./make_info_file.py --help
./make_info_file.py
./make_info_file.py --years 2016 2017
./make_info_file.py --years 2016 2017 --channels D0ToKsKK D0ToKsKPi D0ToKsPiK D0ToKsPiPi
./make_info_file.py --years 2016 2017 --polarities MagDown --tag-types DstarTag SecondaryTag 
./make_info_file.py --years 2016 --polarities MagDown --tag-types SecondaryTagMC --data-types MC --channels D0ToKsPiPi
./make_info_file.py --tag-types SecondaryTag DstarTag DstarLTUNBTag --data-types RealData
./make_info_file.py --tag-types DstarLTUNBTag --data-types RealData --channels D0ToKsPiPi D0ToKsKK 
```

from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import CombineParticles
from GaudiConf import IOHelper

############# Global settings
year = DaVinci().DataType
data = True
if(DaVinci().Simulation == True): data = False
stream = "AllStreams"
if (data):
    stream = "Bhadron"

#### Restrip MC
if(DaVinci().Simulation == True):
    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive
    from Configurables import (
        EventNodeKiller,
        ProcStatusCheck,
        DaVinci,
        DecayTreeTuple
    )
    from DecayTreeTuple.Configuration import *

    # Node killer: remove the previous Stripping
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

    # Build a new stream called 'CustomStream' that only
    # contains the desired line
    strip = 'stripping29'   
    streams = buildStreams(stripping=strippingConfiguration(strip),archive=strippingArchive(strip))

    lineDD = 'B02DKsPiDDD2HHHCFPIDBeauty2CharmLine'
    lineLL = 'B02DKsPiLLD2HHHCFPIDBeauty2CharmLine'
    line_DKsK_DD = 'B02DKsKDDD2HHHCFPIDBeauty2CharmLine'
    line_DKsK_LL = 'B02DKsKLLD2HHHCFPIDBeauty2CharmLine'

    custom_stream = StrippingStream('CustomStream')
    custom_lineDD = 'Stripping'+lineDD 
    custom_lineLL = 'Stripping'+lineLL
    custom_DKsK_lineDD = 'Stripping'+line_DKsK_DD 
    custom_DKsK_lineLL = 'Stripping'+line_DKsK_LL

    for stream in streams:
        for sline in stream.lines:
            if sline.name() == custom_lineDD:
                custom_stream.appendLines([sline])
            if sline.name() == custom_lineLL:
                custom_stream.appendLines([sline])
            if sline.name() == custom_DKsK_lineDD:
                custom_stream.appendLines([sline])
            if sline.name() == custom_DKsK_lineLL:
                custom_stream.appendLines([sline])


    # Create the actual Stripping configurable
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(Streams=[custom_stream],
                   MaxCandidates=2000,
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents)


###############   Pre Filter, does not really do much except choose only candidates passing the Stripping line, maybe beneficial to performance
from Configurables import LoKi__HDRFilter as StripFilter
stripFilter = StripFilter( 'stripPassFilter',\
                          Code = "( HLT_PASS('StrippingB02DKsPiDDD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsPiDDWSD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsPiLLD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsPiLLWSD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsKDDD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsKDDWSD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsKLLD2HHHCFPIDBeauty2CharmLineDecision') | HLT_PASS('StrippingB02DKsKLLWSD2HHHCFPIDBeauty2CharmLineDecision') )", 
                          Location= "/Event/Strip/Phys/DecReports")

############# DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolTISTOS
from Configurables import PrintDecayTree, PrintDecayTreeTool

from Configurables import SubstitutePID
from Configurables import TupleToolDecayTreeFitter, TupleToolTrackIsolation, TupleToolTagging, TupleToolRecoStats, TupleToolKinematic, TupleToolGeometry, TupleToolVtxIsoln
from Configurables import LoKi__Hybrid__TupleTool

# Event filter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import FilterDesktop

from Configurables import CheckPV
checkPVs = CheckPV("checkPVs")
checkPVs.MinPVs = 1
checkPVs.MaxPVs = -1

triggerlines_Run1 = [
                     "L0HadronDecision", 
                     "L0MuonDecision",
                     "L0DiMuonDecision",
                     "L0ElectronDecision",
                     "L0PhotonDecision",
                     "Hlt1TrackAllL0Decision", 
                     'Hlt2IncPhiDecision', 
                     'Hlt2Topo2BodyBBDTDecision', 
                     'Hlt2Topo3BodyBBDTDecision', 
                     'Hlt2Topo4BodyBBDTDecision',
                     ]

triggerlines_Run2 = [
                     "L0HadronDecision", 
                     "L0MuonDecision",
                     "L0DiMuonDecision",
                     "L0ElectronDecision",
                     "L0PhotonDecision",
                     "Hlt1TrackMVADecision",
                     "Hlt1TwoTrackMVADecision",
                     'Hlt2IncPhiDecision', 
                     "Hlt2PhiIncPhiDecision",
                     'Hlt2Topo2BodyDecision',
                     'Hlt2Topo3BodyDecision',
                     'Hlt2Topo4BodyDecision',
                     ]

if DaVinci().DataType in ['2011', '2012']: triggerlines = triggerlines_Run1
else: triggerlines = triggerlines_Run2

line_names = ['B02DKsPiDDD2HHHCFPIDBeauty2CharmLine','B02DKsPiDDWSD2HHHCFPIDBeauty2CharmLine','B02DKsPiLLD2HHHCFPIDBeauty2CharmLine','B02DKsPiLLWSD2HHHCFPIDBeauty2CharmLine']
ntuple_names = [ 'B2DKspi_DD_Tuple','B2DKspi_DD_WS_Tuple','B2DKspi_LL_Tuple','B2DKspi_LL_WS_Tuple' ]

line_names += ['B02DKsKDDD2HHHCFPIDBeauty2CharmLine','B02DKsKDDWSD2HHHCFPIDBeauty2CharmLine','B02DKsKLLD2HHHCFPIDBeauty2CharmLine','B02DKsKLLWSD2HHHCFPIDBeauty2CharmLine']
ntuple_names += [ 'B2DKsK_DD_Tuple','B2DKsK_DD_WS_Tuple','B2DKsK_LL_Tuple','B2DKsK_LL_WS_Tuple' ]

def CreateTreeSequence(intuple,idecay):
    
    ntuple = ntuple_names[intuple]
    line = line_names[intuple]

    if(data): inputs = 'Phys/{0}/Particles'.format(line)
    else: inputs = '/Event/Phys/{0}/Particles'.format(line)

    reqsel = AutomaticData(Location = inputs)
    if(intuple<3): Bs2DsXSel = FilterDesktop("Bs2DsXSel_{0}_{1}".format(intuple,idecay), Code = "(INTREE((ABSID=='B0') & (M>4900) & (BPVDIRA > 0.9999) & (BPVIPCHI2() < 20) & (BPVVDCHI2 > 100) & (CHILD(VFASPF(VZ),1)-VFASPF(VZ)> -1.0*mm) & (CHILD(VFASPF(VZ),3)-VFASPF(VZ)>-1.0*mm) & (CHILD(PROBNNpi,2)>0.1)  )) & (INTREE( (ABSID=='K+') & (PROBNNk>0.1) )) ")
    else: Bs2DsXSel = FilterDesktop("Bs2DsXSel_{0}_{1}".format(intuple,idecay), Code = "(INTREE((ABSID=='B0') & (M>4900) & (BPVDIRA > 0.9999) &\
 (BPVIPCHI2() < 20) & (BPVVDCHI2 > 100) & (CHILD(VFASPF(VZ),1)-VFASPF(VZ)> -1.0*mm) & (CHILD(VFASPF(VZ),3)-VFASPF(VZ)>-1.0*mm) & (CHILD(PROBNNk,2)>0.1)\
    )) & (INGENERATION( (ABSID=='K+') & (PROBNNk>0.1), 2 ))")

    MyFilterSel = Selection("MyFilterSel_{0}_{1}".format(intuple,idecay), Algorithm = Bs2DsXSel, RequiredSelections = [reqsel])

    b2dkspiTuple = DecayTreeTuple(ntuple)
    if idecay == 0:
        b2dkspiTuple.Decay = "[[B0]cc -> ^(D- -> ^K+ ^pi- ^pi-) ^(KS0 -> ^pi+ ^pi-) ^pi+]CC"
        b2dkspiTuple.Branches= {
        "B" : "^([[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) pi+]CC)" ,
        "Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) ^(KS0 -> pi+ pi-) pi+]CC",
        "D" : "[[B0]cc -> ^(D- -> K+ pi- pi-) (KS0 -> pi+ pi-) pi+]CC",
        "pip_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> ^pi+ pi-) pi+]CC",
        "pim_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ ^pi-) pi+]CC",
        "K_D" : "[[B0]cc -> (D- -> ^K+ pi- pi-) (KS0 -> pi+ pi-) pi+]CC",
        "pi1_D" : "[[B0]cc -> (D- -> K+ ^pi- pi-) (KS0 -> pi+ pi-) pi+]CC",
        "pi2_D" : "[[B0]cc -> (D- -> K+ pi- ^pi-) (KS0 -> pi+ pi-) pi+]CC",
        "pi" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) ^pi+]CC",
        }
    if idecay == 1:
        b2dkspiTuple.Decay = "[[B0]cc -> ^(D- -> ^K+ ^pi- ^pi-) ^(KS0 -> ^pi+ ^pi-) ^pi-]CC"
        b2dkspiTuple.Branches= {
        "B" : "^([[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) pi-]CC)" ,
        "Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) ^(KS0 -> pi+ pi-) pi-]CC",
        "D" : "[[B0]cc -> ^(D- -> K+ pi- pi-) (KS0 -> pi+ pi-) pi-]CC",
        "pip_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> ^pi+ pi-) pi-]CC",
        "pim_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ ^pi-) pi-]CC",
        "K_D" : "[[B0]cc -> (D- -> ^K+ pi- pi-) (KS0 -> pi+ pi-) pi-]CC",
        "pi1_D" : "[[B0]cc -> (D- -> K+ ^pi- pi-) (KS0 -> pi+ pi-) pi-]CC",
        "pi2_D" : "[[B0]cc -> (D- -> K+ pi- ^pi-) (KS0 -> pi+ pi-) pi-]CC",
        "pi" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) ^pi-]CC",
    }
    if idecay == 2:
        b2dkspiTuple.Decay = "[[B0]cc -> ^(D- -> ^K+ ^pi- ^pi-) ^(KS0 -> ^pi+ ^pi-) ^K+]CC"
        b2dkspiTuple.Branches= {
        "B" : "^([[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) K+]CC)" ,
        "Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) ^(KS0 -> pi+ pi-) K+]CC",
        "D" : "[[B0]cc -> ^(D- -> K+ pi- pi-) (KS0 -> pi+ pi-) K+]CC",
        "pip_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> ^pi+ pi-) K+]CC",
        "pim_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ ^pi-) K+]CC",
        "K_D" : "[[B0]cc -> (D- -> ^K+ pi- pi-) (KS0 -> pi+ pi-) K+]CC",
        "pi1_D" : "[[B0]cc -> (D- -> K+ ^pi- pi-) (KS0 -> pi+ pi-) K+]CC",
        "pi2_D" : "[[B0]cc -> (D- -> K+ pi- ^pi-) (KS0 -> pi+ pi-) K+]CC",
        "pi" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) ^K+]CC",
        }
    if idecay == 3:
        b2dkspiTuple.Decay = "[[B0]cc -> ^(D- -> ^K+ ^pi- ^pi-) ^(KS0 -> ^pi+ ^pi-) ^K-]CC"
        b2dkspiTuple.Branches= {
        "B" : "^([[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) K-]CC)" ,
        "Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) ^(KS0 -> pi+ pi-) K-]CC",
        "D" : "[[B0]cc -> ^(D- -> K+ pi- pi-) (KS0 -> pi+ pi-) K-]CC",
        "pip_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> ^pi+ pi-) K-]CC",
        "pim_Ks" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ ^pi-) K-]CC",
        "K_D" : "[[B0]cc -> (D- -> ^K+ pi- pi-) (KS0 -> pi+ pi-) K-]CC",
        "pi1_D" : "[[B0]cc -> (D- -> K+ ^pi- pi-) (KS0 -> pi+ pi-) K-]CC",
        "pi2_D" : "[[B0]cc -> (D- -> K+ pi- ^pi-) (KS0 -> pi+ pi-) K-]CC",
        "pi" : "[[B0]cc -> (D- -> K+ pi- pi-) (KS0 -> pi+ pi-) ^K-]CC",
    }

    b2dkspiTuple.ReFitPVs = True

    #config tools
    b2dkspiTuple.ToolList +=  ["TupleToolGeometry", \
                                "TupleToolKinematic", \
                                "TupleToolPrimaries", \
                                #"TupleToolEventInfo", \
                                "TupleToolTrackInfo", \
                                "TupleToolRecoStats", 
                                "TupleToolPid"]

    if (data==False):
        b2dkspiTuple.ToolList +=  [
                                "TupleToolMCTruth", \
                                "TupleToolMCBackgroundInfo",
                                "TupleToolPhotonInfo"
                    ]
        MCTruth = TupleToolMCTruth()
        MCTruth.ToolList =  [
             "MCTupleToolHierarchy"
            , "MCTupleToolKinematic"
            , "MCTupleToolReconstructed"
            ]
        b2dkspiTuple.addTool(MCTruth) 
        
        b2dkspiTuple.ToolList +=  ["TupleToolTrackIsolation"]
        b2dkspiTuple.addTool(TupleToolTrackIsolation, name="TupleToolTrackIsolation")
        b2dkspiTuple.TupleToolTrackIsolation.FillAsymmetry = True
        b2dkspiTuple.TupleToolTrackIsolation.FillDeltaAngles = False
        b2dkspiTuple.TupleToolTrackIsolation.MinConeAngle = 1.0

    b2dkspiTuple.addTool( TupleToolKinematic,name = "TupleToolKinematic" )
    b2dkspiTuple.TupleToolKinematic.Verbose = False

    b2dkspiTuple.addTool( TupleToolGeometry, name = "TupleToolGeometry" )
    b2dkspiTuple.TupleToolGeometry.Verbose = True
    b2dkspiTuple.TupleToolGeometry.RefitPVs = True
    #b2dkspiTuple.TupleToolGeometry.FillMultiPV = True

    b2dkspiTuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    b2dkspiTuple.TupleToolRecoStats.Verbose = True
    b2dkspiTuple.UseLabXSyntax = True                          
    b2dkspiTuple.RevertToPositiveID = False

    b2dkspiTuple.addTool(TupleToolDecay, name="B")
    b2dkspiTuple.addTool(TupleToolDecay, name="Ks")
    b2dkspiTuple.addTool(TupleToolDecay, name="D")

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("DTF"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/DTF" ]
    b2dkspiTuple.B.DTF.constrainToOriginVertex = True
    b2dkspiTuple.B.DTF.daughtersToConstrain = ["D-", "KS0"]
    b2dkspiTuple.B.DTF.UpdateDaughters = True
    b2dkspiTuple.B.DTF.Verbose = True

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("KsDTF"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/KsDTF" ]
    b2dkspiTuple.B.KsDTF.constrainToOriginVertex = True
    b2dkspiTuple.B.KsDTF.daughtersToConstrain = ["KS0"]
    #b2dkspiTuple.B.KsDTF.UpdateDaughters = True
    b2dkspiTuple.B.KsDTF.Verbose = True

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("DDTF"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/DDTF" ]
    b2dkspiTuple.B.DDTF.constrainToOriginVertex = True
    b2dkspiTuple.B.DDTF.daughtersToConstrain = ["D-"]
    #b2dkspiTuple.B.DDTF.UpdateDaughters = True
    b2dkspiTuple.B.DDTF.Verbose = True

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("BDTF"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/BDTF" ]
    b2dkspiTuple.B.BDTF.constrainToOriginVertex = True
    b2dkspiTuple.B.BDTF.daughtersToConstrain = ["B0"]
    #b2dkspiTuple.B.BDTF.UpdateDaughters = True                                                                                                         
    b2dkspiTuple.B.BDTF.Verbose = True

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("FullDTF"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/FullDTF" ]         
    b2dkspiTuple.B.FullDTF.constrainToOriginVertex = True
    b2dkspiTuple.B.FullDTF.daughtersToConstrain = ["D-", "B0", "KS0"]  
    b2dkspiTuple.B.FullDTF.UpdateDaughters = True
    b2dkspiTuple.B.FullDTF.Verbose = True

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("FullBsDTF"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/FullBsDTF" ]
    b2dkspiTuple.B.FullBsDTF.constrainToOriginVertex = True
    b2dkspiTuple.B.FullBsDTF.Substitutions = { 'B0 -> Charm Hadron Hadron' : 'B_s0' , 'B~0 -> Charm Hadron Hadron' : 'B_s~0'  }
    b2dkspiTuple.B.FullBsDTF.daughtersToConstrain = ["D-", "B_s0", "KS0"]
    b2dkspiTuple.B.FullBsDTF.UpdateDaughters = True
    b2dkspiTuple.B.FullBsDTF.Verbose = True

    b2dkspiTuple.B.addTool(TupleToolDecayTreeFitter("PV"))
    b2dkspiTuple.B.ToolList +=  ["TupleToolDecayTreeFitter/PV" ]         
    b2dkspiTuple.B.PV.constrainToOriginVertex = True 
    b2dkspiTuple.B.PV.UpdateDaughters = True
    b2dkspiTuple.B.PV.Verbose = True

    LoKiTool = b2dkspiTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
    LoKiTool.Variables = { "ETA" : "ETA" };

    LoKiToolB = b2dkspiTuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolB")
    LoKiToolB.Variables = { 
                           "DOCA1" : "DOCA(1,2)", "DOCA2" : "DOCA(1,3)","DOCA3" : "DOCA(2,3)", "TAU" : "BPVLTIME()", "TAUERR" : "BPVLTERR()", "ptasy": ("RELINFO('/Event/Bhadron/Phys/{0}/P2ConeVar3','CONEPTASYM', -1000.)".format(line)), "ptasy_15": ("RELIN\
FO('/Event/Bhadron/Phys/{0}/P2ConeVar1','CONEPTASYM', -1000.)".format(line)), "ptasy_17": ("RELIN\
FO('/Event/Bhadron/Phys/{0}/P2ConeVar2','CONEPTASYM', -1000.)".format(line))
                            };

    LoKiToolKs = b2dkspiTuple.Ks.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolKs")
    LoKiToolKs.Variables = {
                           "DOCA1" : "DOCA(1,2)", "TAU" : "BPVLTIME()", "TAUERR" : "BPVLTERR()"
                            };

    LoKiToolD = b2dkspiTuple.D.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolD")
    LoKiToolD.Variables = {
                           "DOCA1" : "DOCA(1,2)", "DOCA2" : "DOCA(1,3)","DOCA3" : "DOCA(2,3)", "TAU" : "BPVLTIME()", "TAUERR" : "BPVLTERR()"
                            };

    if idecay in [0,2]:
            if(data):
                tupletooltagging = b2dkspiTuple.addTupleTool(TupleToolTagging('tupletooltagging'))
                if year not in ['2011', '2012']:
                    tupletooltagging.ActiveTaggerTypes = [
                        "OSCharm", "OSElectron", "OSElectronLatest", "OSKaon",
                        "OSKaonLatest", "OSMuon", "OSMuonLatest", "OSVtxCh", "SSKaon",
                        "SSKaonLatest", "SSPion","SSProton"]
                    tupletooltagging.Verbose = True
                tupletooltagging.UseFTfromDST=True
            else:
                from Configurables import BTaggingTool
                tt_tagging = b2dkspiTuple.addTupleTool("TupleToolTagging") 
                tt_tagging.Verbose = True
                tt_tagging.AddMVAFeatureInfo = False
                tt_tagging.AddTagPartsInfo = False 
                btagtool = tt_tagging.addTool(BTaggingTool , name = "MyBTaggingTool")
                from FlavourTagging.Tunings import applyTuning as applyFTTuning # pick the right tuning here ...
                applyFTTuning(btagtool , tuning_version="Summer2017Optimisation_v4_Run2")
                tt_tagging.TaggingToolName = btagtool.getFullName ()

    #trigger config
    b2dkpipitt = b2dkspiTuple.B.addTupleTool(TupleToolTISTOS)
    b2dkpipitt.TriggerList = triggerlines
    b2dkpipitt.FillL0 = True
    b2dkpipitt.FillHlt1 = True
    b2dkpipitt.FillHlt2 = True
    b2dkpipitt.Verbose = True
    b2dkpipitt.VerboseL0 = True
    b2dkpipitt.VerboseHlt1 = True
    b2dkpipitt.VerboseHlt2 = True

    #main sequence
    makeb2dkspiseq = SelectionSequence("makeb2dkspiseq_{0}_{1}".format(intuple,idecay), TopSelection = MyFilterSel)
    b2dkspiTuple.Inputs = [makeb2dkspiseq.outputLocation()]
    b2dkspiseq = GaudiSequencer("B2dkspiSeq_{0}_{1}".format(intuple,idecay))
    b2dkspiseq.Members += [makeb2dkspiseq.sequence(),b2dkspiTuple]

    return b2dkspiseq

seq_DD = CreateTreeSequence(0,0)
seq_DD_WS = CreateTreeSequence(1,1)
seq_LL = CreateTreeSequence(2,0)
seq_LL_WS = CreateTreeSequence(3,1)

seq_DKsK_DD = CreateTreeSequence(4,2)
seq_DKsK_DD_WS = CreateTreeSequence(5,3)
seq_DKsK_LL = CreateTreeSequence(6,2)
seq_DKsK_LL_WS = CreateTreeSequence(7,3)

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---


if(data):
 DaVinci().InputType = 'MDST'
 DaVinci().RootInTES = '/Event/{0}'.format(stream) #to be used for micro-DSTs
else:
 DaVinci().InputType = 'DST'

if(data): DaVinci().EventPreFilters = [stripFilter]
else: DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])

DaVinci().UserAlgorithms += [MomentumCorrection(DaVinci().Simulation)]
if(data): DaVinci().UserAlgorithms += [ seq_DD,seq_DD_WS,seq_LL,seq_LL_WS]
if(data): DaVinci().UserAlgorithms += [ seq_DKsK_DD, seq_DKsK_DD_WS,   seq_DKsK_LL, seq_DKsK_LL_WS]
else: DaVinci().UserAlgorithms += [ seq_DD, seq_LL, seq_DKsK_DD,seq_DKsK_LL]

DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 50000
DaVinci().TupleFile = "b2dkspi.root"
if(data): DaVinci().Lumi = True
else: DaVinci().Lumi = False

#IOHelper().inputFiles([
#'./00040648_00000026_2.AllStreams.dst'
#'../../makeTuple/00071501_00000026_1.bhadron.mdst'
#], clear=True)

from Configurables import DaVinci
DaVinci().DataType = "2015"
DaVinci().Simulation = False

from Configurables import CondDB, CondDBAccessSvc
CondDB().LatestGlobalTagByDataType = DaVinci().DataType

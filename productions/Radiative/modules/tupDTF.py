"""
DTF instance creation
"""
from Configurables import DecayTreeTuple
from Configurables import TupleToolDecayTreeFitter
from Configurables import TupleToolDecay
import sys

def  addInstanceDTF(tuple,instance='Fit',branch='', verbose = False ,FullInfo=True, Daughters=True,ConstVtx=False,ConstMass=[]) :
    if instance == '' :
        print 'An DTF instance name is required'
        sys.exit(-1)

    if branch != '' :    
        tuple.addTool(TupleToolDecay,name=branch)
        exec('b = tuple.'+branch)
        b.ToolList+=['TupleToolDecayTreeFitter/'+instance]
        b.addTool(TupleToolDecayTreeFitter,name=instance)
        exec('tool=tuple.'+branch+'.'+instance)
    else :
        tuple.addTool(TupleToolDecayTreeFitter,name=instance)
        tuple.ToolList+=['TupleToolDecayTreeFitter/'+instance]
        exec('tool=tuple.'+instance)

    tool.Verbose=FullInfo
    tool.UpdateDaughters=Daughters
    tool.constrainToOriginVertex=ConstVtx
    if ConstMass != [] : tool.daughtersToConstrain=ConstMass
    if verbose : print "Created DTF instance '"+tool.getName()+"' :  [Verbose="+str(FullInfo)+", updateDaughters="+str(Daughters)+", VertexConstraint="+str(ConstVtx)+", MassConstraint(s)=["+','.join(ConstMass)+"]]"

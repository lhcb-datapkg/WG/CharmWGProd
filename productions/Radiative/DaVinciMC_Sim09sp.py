"""
Standard MC DaVinci construction for Radiative Ntuples

SIM tags have to be checked, mainly Sim09h version, some of 2016 Sim09X may use this script 
Careful with 2016 tags !!!!
"""

from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import CondDB

# Setup environment options
os.environ['DATATYPE'] = 'MC'

# Import environment variables
year = 		os.getenv("YEAR")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP")

DaVinci().Simulation   = True
DaVinci().Lumi         = False
DaVinci().DataType = year  

print '------------------------------'
magnet = 'md'  # set magdown as default
polarity=os.getenv('POLARITY','unset')
if polarity == 'up' :
    magnet='mu'
elif polarity == 'unset' :
    print 'Magnet polarity should be set for MC - assume MagDown'

# ==== dictionary for latest db ==== #
sim = {}

# -- sim09 special  [Update on 2019/26/09]
sim['2012,reco14++']='sim-20160321-2'
sim['2016,reco14++']='sim-20170721-1'
sim['2017,reco14++']='sim-20190430-1'
sim['2018,reco14++']='sim-20190430'

dddb={} #[update on 2017/21/09] 
dddb['2010']='dddb-20170721'   # 2010   
dddb['2011']='dddb-20170721-1' # 2011 
dddb['2012']='dddb-20150928' # 2012/2013 
dddb['2013']='dddb-20170721-2'
dddb['2015']='dddb-20170721-3' # 2015/2016/2017
dddb['2016']='dddb-20170721-3' 
dddb['2017']='dddb-20170721-3' # as for 2017/21/09 
dddb['2018']='dddb-20170721-3'     

# -------- apply DB settings
calo='reco14++'
# temporary (workaround for Calo2MC matching bug - assume rawBanks are available )
if stripping.find('21') != -1 :
    from Configurables import ToolSvc,Calo2MCTool
    ToolSvc().addTool(Calo2MCTool,name="Calo2MCTool")
    ToolSvc().Calo2MCTool.Hypo2Cluster=True


#===    
              
DaVinci().DDDBtag = dddb[year]

DaVinci().CondDBtag = sim[year+','+calo]+'-vc-'+magnet+'100'

print ' - DB setting for '+year+' MC / stripping='+stripping+' / calo='+calo+' / polarity = '+polarity
print '   - SimCond  : '+DaVinci().CondDBtag
print '   - DDDB     : '+DaVinci().DDDBtag


# == CaloReprocessing in case of re-stripping on MC ==#
if calo == 'reco14++' and DaVinci().getProp("Simulation") and restrip == True :
    print "  ! CaloReprocessing is activated for the reStripping..."
    from Configurables import PhysConf
    PhysConf().CaloReProcessing=True
else:
    print "CaloReprocessing is OFF ..."
print '------------------------------'

# -- L0Monitoring on MC

from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import L0DUReportMonitor
l0moni = L0DUReportMonitor("L0DUMoni")
from Configurables import GaudiSequencer
DaVinci().UserAlgorithms  += [l0moni]


# == get stripping version


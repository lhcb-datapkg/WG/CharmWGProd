import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci, CondDB
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay


from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger

from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos

from Configurables import LoKi__Hybrid__TupleTool
#from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool

from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",   
    "TupleToolPropertime",
    #"TupleToolPrimaries", ## added afterwards to configure it
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    #"TupleToolTISTOS",  ## added afterwards to configure it 
    #"TupleToolTrigger"  ## added afterwards to configure it 
    ]

TriggerList = [
    "L0B1gasDecision"     
    ,"L0B2gasDecision"     
    ,"L0DiEMDecision,lowMultDecision"
    ,"L0DiHadron,lowMultDecision"   
    ,"L0DiMuonDecision"  
    ,"L0DiMuon,lowMultDecision" 
    ,"L0ElectronDecision"          
    ,"L0Electron,lowMultDecision"    
    ,"L0HadronDecision"    
    ,"L0JetElDecision"    
    ,"L0JetPhDecision" 
    ,"L0MuonDecision"  
    ,"L0Muon,lowMultDecision"   
    ,"L0MuonEWDecision"       
    ,"L0PhotonDecision"                                              
    ,"L0Photon,lowMultDecision"                        
    ,"Hlt1TrackMVADecision"
    ,"Hlt1TrackMVALooseDecision"
    ,"Hlt1TrackMVATightDecision"
    ,"Hlt1TwoTrackMVADecision"
    ,"Hlt1TwoTrackMVALooseDecision"
    ,"Hlt1TwoTrackMVATightDecision"
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2KS0h=LoKi__Hybrid__TupleTool("LoKi_Dplus2KS0h")
LoKi_Dplus2KS0h.Variables =  {
    'DOCACHI2_KS0_hplus' : 'DOCACHI2(1,2)',
    'DOCA_KS0_hplus'     : 'DOCA(1,2)',
    'SUMPTChildren' : '((CHILD(1, PT)) + (CHILD(2, PT)))',
    #'BPVVDCHI2'     : 'BPVVDCHI2',                      
    #'BPVDIRA'       : 'BPVDIRA',                         
    'VCHI2PDOF'     : 'VFASPF(VCHI2PDOF)',                          
    'BPVLTIME'      : 'BPVLTIME()'                 
  }
LoKi_KS0=LoKi__Hybrid__TupleTool("LoKi_KS0")
LoKi_KS0.Variables = {
   'DOCACHI2_piplus_piminus' : 'DOCACHI2(1,2)',
   'DOCA_piplus_piminus'     : 'DOCA(1,2)',
   'SUMPTChildren' : '((CHILD(1, PT)) + (CHILD(2, PT)))',
  }

LoKi_Dplus2KPiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2KPiPi")          
LoKi_Dplus2KPiPi.Variables = {                                           
    'DOCA_Kminus_piplus1'    : 'DOCA(1,2)',                                
    'DOCA_Kminus_piplus2'    : 'DOCA(1,3)',                                                
    'DOCA_piplus1_piplus2'   : 'DOCA(2,3)',                 
    'DOCACHI2_Kminus_piplus1'    : 'DOCACHI2(1,2)',                                
    'DOCACHI2_Kminus_piplus2'    : 'DOCACHI2(1,3)',                                                
    'DOCACHI2_piplus1_piplus2'   : 'DOCACHI2(2,3)',                 
    'SUMPTChildren' : '((CHILD(1, PT)) + (CHILD(2, PT)) + (CHILD(3, PT)))',              
    #'BPVVDCHI2'     : 'BPVVDCHI2',                                   
    #'BPVDIRA'       : 'BPVDIRA',                                  
    'VCHI2PDOF'     : 'VFASPF(VCHI2PDOF)',                                    
    'BPVLTIME'      : 'BPVLTIME()'                          
    }                                       


################# Inputs and Momentum Scaling Correction  ######################
year = DaVinci().DataType
print year

lineKS0PiLL = "Hlt2CharmHadDp2KS0Pip_KS0LLTurbo"
lineKS0PiDD = "Hlt2CharmHadDp2KS0Pip_KS0DDTurbo"
lineKPiPi = "Hlt2CharmHadDpToKmPipPipTurbo"

from PhysConf.Selections import AutomaticData
AD_KS0PiLL = AutomaticData( Location = lineKS0PiLL + '/Particles' )
AD_KS0PiDD = AutomaticData( Location = lineKS0PiDD + '/Particles' )
AD_Kpipi   = AutomaticData( Location = lineKPiPi   + '/Particles' )


from PhysConf.Selections import MomentumScaling
KS0PiLL = MomentumScaling ( AD_KS0PiLL , Turbo = True , Year = year )
KS0PiDD = MomentumScaling ( AD_KS0PiDD , Turbo = True , Year = year )
Kpipi   = MomentumScaling ( AD_Kpipi   , Turbo = True , Year = year ) 

from PhysConf.Selections import TupleSelection


################# D -> KS0 H #####################################
tuple_KsPiLL = TupleSelection ( 
    'Dp2KS0pipLL' , 
    [KS0PiLL], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        },
    ToolList = [ ] 
    )

tuple_KsPiDD = TupleSelection ( 
    'Dp2KS0pipDD' , 
    [KS0PiDD], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        },
    ToolList = [ ] 
    )

## D -> KS0 H
for dtt in [tuple_KsPiLL,tuple_KsPiDD]:
    
    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="KS0")
    dtt.addTool(TupleToolDecay, name="piplus")
    dtt.addTool(TupleToolDecay, name="piminus")
    dtt.addTool(TupleToolDecay, name="hplus")
 #LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.KS0,dtt.piplus,dtt.piminus,dtt.hplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2KS0h)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KS0h"]
    dtt.KS0.addTool(LoKi_KS0)
    dtt.KS0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_KS0"]

#DECAY TREE FITTER
#constrain PV and mass                                          
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTF')                     
    dtt.Dplus.DTF.Verbose = True                              
    dtt.Dplus.DTF.constrainToOriginVertex = True                
    dtt.Dplus.DTF.daughtersToConstrain = ['KS0']                         
    dtt.Dplus.DTF.UpdateDaughters = True                  
#constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True
        
################# D -> K Pi Pi  #####################################


tuple_KPiPi = TupleSelection ( 
    'Dp2KmPipPip' , 
    [Kpipi], 
    Decay = "[D+ -> ^K-  ^pi+ ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> K-  pi+ pi+]CC",
        "Kminus"  : "[D+ -> ^K-  pi+ pi+]CC",
        "piplus1" : "[D+ -> K-  ^pi+ pi+]CC",
        "piplus2" : "[D+ -> K-  pi+ ^pi+]CC"
        },
    ToolList = [ ] 
    )


for dtt in [tuple_KPiPi]:
    
    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="Kminus")
    dtt.addTool(TupleToolDecay, name="piplus1")
    dtt.addTool(TupleToolDecay, name="piplus2")

#LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.Kminus,dtt.piplus1,dtt.piplus2]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2KPiPi)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KPiPi"]
   
#DECAY TREE FITTER
 #constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True
#DTF with constraint on Dplus mass (for Dalitz)
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyM') 
    dtt.Dplus.DTFonlyM.Verbose = True
    dtt.Dplus.DTFonlyM.daughtersToConstrain = ['D+']
    dtt.Dplus.DTFonlyM.UpdateDaughters = True

################# D -> KS0 H, D-> Phi Pi #####################################

for dtt in [tuple_KsPiLL,tuple_KsPiDD,tuple_KPiPi]:

    dtt.TupleName = "ntp"

    dtt.ToolList =  TupleToolList
 
    triggerTool = dtt.addTupleTool("TupleToolTrigger")
    triggerTool.VerboseL0   = True
    triggerTool.VerboseHlt1 = True
    triggerTool.FillHlt2    = False
    triggerTool.VerboseHlt2 = False
    triggerTool.TriggerList = TriggerList

    tistosTool = dtt.addTupleTool("TupleToolTISTOS")
    tistosTool.VerboseL0   = True
    tistosTool.VerboseHlt1 = True
    tistosTool.VerboseHlt2 = True
    tistosTool.FillHlt2    = False
    tistosTool.TriggerList = TriggerList

    primTool = dtt.addTupleTool("TupleToolPrimaries")
    primTool.Verbose = True
    
    if year == "2015":
        dtt.WriteP2PVRelations = False
        dtt.InputPrimaryVertices = '/Event/Turbo/Primary'
        primTool.InputLocation = 'Primary'

from PhysConf.Selections import SelectionSequence
seq_KsPiLL = SelectionSequence( 'Seq_KsPiLL' , tuple_KsPiLL ).sequence()    
seq_KsPiDD = SelectionSequence( 'Seq_KsPiDD' , tuple_KsPiDD ).sequence()    
seq_KPiPi  = SelectionSequence( 'seq_KPiPi' ,  tuple_KPiPi ).sequence()    

############ DaVinci configuration ###############################

from Configurables import DstConf # necessary for DaVinci v40r1 onwards
DstConf().Turbo = True

# FIX for DaVinci mess
from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()
from Configurables import Gaudi__DataLink as Link
rawEvt1 = Link ( 'LinkRawEvent1', 
                 What   =  '/Event/DAQ/RawEvent' , 
                 Target = '/Event/Trigger/RawEvent' )
dod.AlgMap [ rawEvt1  . Target ] = rawEvt1

from Configurables import LoKi__HDRFilter as Filter
hltfilter = Filter('HLT2Filter', Code = "HLT_PASS_RE('"+lineKPiPi+"Decision')")

if year == "2015" or year == "2016":
    rootInTES = '/Event/Turbo'
elif year == "2017" or year == "2018":
    rootInTES = '/Event/Charmspec/Turbo'

if year == "2015":
    from Configurables import DstConf
    DstConf().Turbo = True

    
DaVinci().InputType       = 'MDST'
DaVinci().Turbo           = True 
DaVinci().RootInTES       = rootInTES 
DaVinci().DataType        = year 
DaVinci().Simulation      = False 
DaVinci().EventPreFilters = [ hltfilter ] 
DaVinci().UserAlgorithms  = [ seq_KPiPi ] 
DaVinci().EvtMax          = -1
DaVinci().Lumi            = not DaVinci().Simulation
DaVinci().TupleFile       = "tuple_D2KPiPi.root" 
DaVinci().PrintFreq       = 50000 


CondDB  ( LatestGlobalTagByDataType = year )
################# READ FROM LOCAL FILE ###########################  
#from GaudiConf import IOHelper
#IOHelper().inputFiles([
    #"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000085_1.turbo.mdst" ##2015
#        "root://guppy5.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/LHCb/Collision16/CHARMSPECPARKED.MDST/00076510/0000/00076510_00000138_1.charmspecparked.mdst" ##2016
    #        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMSPEC.MDST/00064782/0000/00064782_00000387_1.charmspec.mdst" ##2017
#   "root://marsedpm.in2p3.fr:1094//dpm/in2p3.fr/home/lhcb/LHCb/Collision18/CHARMSPEC.MDST/00075226/0000/00075226_00009035_1.charmspec.mdst" ##2018
#], clear = True)

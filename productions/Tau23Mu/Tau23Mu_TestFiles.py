# DV options
from Configurables import DaVinci
data_type = DaVinci().DataType
IsMC=DaVinci().Simulation

# Test files
files = {
    # Data Prompt
    #'2015':["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00047765/0000/00047765_00001201_1.turbo.mdst"],
    '2016': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/LEPTONIC.MDST/00069599/0000/00069599_00000152_1.leptonic.mdst"],
    #'2017': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMMULTIBODY.MDST/00064380/0000/00064380_00001831_1.charmmultibody.mdst"],
    '2018': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00092361/0000/00092361_00001239_1.leptonic.mdst"],
    # MC Prompt
    '2016_MC': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/MCTRUE.DST/00082647/0000/00082647_00000018_1.mctrue.dst"],
    '2018_MC': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2018/MCTRUE.DST/00088974/0000/00088974_00000026_1.mctrue.dst"],
}
dt_opts = ''
if IsMC: dt_opts+='_MC'
DaVinci().Input = files[data_type+dt_opts]
DaVinci().EvtMax=10000 # testing
DaVinci().InputType='DST' if IsMC else 'MDST'

########################################
# User configuration for 2016 data tuple
########################################
from Configurables import DaVinci
# Data-taking year
DaVinci().DataType = '2018'
# data or MC?
DaVinci().Simulation = False
DaVinci().InputType='MDST'

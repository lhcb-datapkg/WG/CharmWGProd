########################################
# User configuration for 2016 data tuple
########################################
from Configurables import DaVinci
# Data-taking year
DaVinci().DataType = '2015'
# data or MC?
DaVinci().Simulation = False
DaVinci().InputType='MDST'

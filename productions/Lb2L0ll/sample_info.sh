# Run dirac-bookkeeping-decays-path 
# over each eventtype to make overview of available samples
# Print to separate logs per eventtype

eventtypes=(
    15114102 # Lmm
    15114113 # Lme
    15124102 # Lee
    15144103 # LJpsimm
    15144111 # LPsimm
    15154105 # LJpsiee
    15154115 # LPsiee

    11114101 # Ksmm
    11124101 # Ksee
    11144103 # KsJpsimm
    11144110 # KsPsimm
    11154100 # KsJpsiee
    11154110 # KsPsiee  

    12143001 # KJpsimm
    12153001 # KJpsiee
)

proxy="lb-run -c best LHCbDirac/prod lhcb-proxy-init"

$proxy

run="lb-run -c best LHCbDirac/prod dirac-bookkeeping-decays-path"

directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sample_dir=$directory/samples
echo $sample_dir
if [ ! -d $sample_dir ]
then
    echo "Sample dir does not exist:"
    mkdir $sample_dir 
fi

for eventtype in "${eventtypes[@]}"; do 
    echo "$run"
    echo "$eventtype" 
    $run $eventtype > $sample_dir/$eventtype.log
done

import sys
import os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')
from Configurables import DaVinci, GaudiSequencer
from helpers import tuple_maker

# ONLY CONFIG NEEDED
tuple_name     = 'Lb2JpsiL_eeTuple' # BECAUSE THIS RECONSTRUCTS Lb2Lee with Jpsi resonance

tuple_maker.tuple_maker( tuple_name, upstream_electrons=True) #False )
#dtt = tuple_seq.Members[-1]

# Add the tuple sequence to the main sequence
#seq = tuple_maker.tuple_sequence()
#seq.Members += [tuple_seq]

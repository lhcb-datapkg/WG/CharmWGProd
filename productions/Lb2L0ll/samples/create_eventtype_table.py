import glob
import os
import sys

# Glob all the sample files
sample_logs = glob.glob("./*.log")

# Create lists for every eventtype, split by WG
RD_ETs      = ["11114101", "11124101", "15114102", "15114113", "15124102", ]
B2CC_ETs    = ["11144103", "11144110", "11154100", "11154110", "15144103", "15144103", "15154105", "15154115", "15144111"]
RD_dict     = {}
B2CC_dict   = {}

for ET in RD_ETs:
    RD_dict[ET] = []
for ET in B2CC_ETs:
    B2CC_dict[ET] = []

# Loop over all the logs 
for log in sample_logs:
    with open(log, 'r') as log_file:
        file_lines = log_file.readlines()

    # Loop over lines in file
    for line in file_lines:
        line = eval(line)

        # Only care about Sim09h, want to skip GAUSSHIST
        if "GAUSSHIST" in line[0]:
            print("Line is GAUSSHIST, skipping for now")
            continue
        elif "Sim09h" not in line[0]:
            continue
        elif line[1] == None:
            continue
        else:
            # Check if it's a B2CC or RD ET
            split_line = line[0].split("/")
            #print(split_line)
            eventtype = split_line[-2]
            productionID = line[5]
            if int(productionID) < 100000: continue # Skip the old productions

            # Append productionID to eventtype
            if eventtype in RD_ETs:
                RD_dict[eventtype].append(productionID)
            elif eventtype in B2CC_ETs:
                B2CC_dict[eventtype].append(productionID)
            else:
                print("\n\nEventtype {} with productionID {} is not RD or B2CC, please check!".format(eventtype, productionID))
                print(line[0])


# Need to pad lists with empty entries to create a DF out of it
def pad_dict_list(dict_list, padel):
    lmax = 0
    for lname in dict_list.keys():
        lmax = max(lmax, len(dict_list[lname]))
    for lname in dict_list.keys():
        ll = len(dict_list[lname])
        if  ll < lmax:
            dict_list[lname] += [padel] * (lmax - ll)
    return dict_list

#print(RD_dict)
#print(B2CC_dict)

# Create dataframes
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

df_RD = pd.DataFrame.from_dict(pad_dict_list(RD_dict, ""))
df_B2CC = pd.DataFrame.from_dict(pad_dict_list(B2CC_dict,""))

print(df_RD)
print(df_B2CC)

# Define plots for the table and save as PDF
fig, ax =plt.subplots(figsize=(12,4))
ax.axis('tight')
ax.axis('off')
the_table = ax.table(cellText=df_RD.values, colLabels=df_RD.columns,loc='center')
pp = PdfPages("RD_MC_eventtypes_prodID.pdf")
pp.savefig(fig, bbox_inches='tight')
pp.close()

fig, ax =plt.subplots(figsize=(12,4))
ax.axis('tight')
ax.axis('off')
the_table = ax.table(cellText=df_B2CC.values, colLabels=df_B2CC.columns,loc='center')
pp = PdfPages("B2CC_MC_eventtypes_prodID.pdf")
pp.savefig(fig, bbox_inches='tight')
pp.close()
"""Set up the main and tuple sequences.

The MainSeq sequence is set as the sole member of DaVinci().UserAlgorithms.
Ntuple algorithms should be added to the TupleSeq sequence, which runs in
non-lazy OR mode.

The DaVinci().Simulation property must be set before this options file is run.
"""
import os
import sys
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')

from Configurables import (
    DaVinci,
    GaudiSequencer,
)


from helpers import tuple_maker#, tuple_maker_Bp, tuple_maker_Bz
main_seq = GaudiSequencer('MainSeq')
main_seq.Members = [
    tuple_maker.tuple_sequence(),
#    tuple_maker_Bp.tuple_sequence(),
#    tuple_maker_Bz.tuple_sequence()
]

DaVinci().UserAlgorithms = [main_seq]

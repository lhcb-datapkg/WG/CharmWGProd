from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *

#####################################################################
#
# Define template tuple
#
######################################################################

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
TupTmp          = DecayTreeTuple()
######################################################################
from Configurables import TupleToolDecay

TupTmp.addTool( TupleToolDecay, name = 'Dst' )
TupTmp.addTool( TupleToolDecay, name = "D0" )


######################################################################
TupTmp.ToolList += [
    "TupleToolANNPID",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolPrimaries",
    "TupleToolPropertime",
    "TupleToolRecoStats",
    "TupleToolTrackInfo",
    "TupleToolMuonPid",
    "TupleToolAngles"
    ]

######################################################################
from Configurables import TupleToolTISTOS

TupTmp.addTool( TupleToolTISTOS, name = "L0TISTOS" )
TupTmp.ToolList += [ "TupleToolTISTOS/L0TISTOS" ]
TupTmp.L0TISTOS.TriggerList = [
    "L0ElectronDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0DiMuonDecision",
    "L0PhotonDecision",

    "Hlt1TrackMuonDecision",
    "Hlt1TrackMVADecision",
    "Hlt1TrackMVALooseDecision",
    "Hlt1TrackMuonMVADecision",
    "Hlt1TwoTrackMVALooseDecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1SingleMuonHighPTDecision",
    "Hlt1DiMuonHighMassDecision",
    "Hlt1DiMuonLowMassDecision",
    "Hlt1DiMuonNoL0Decision",
    "Hlt1DiMuonNoIPDecision",
    "Hlt1DiMuonNoIPSSDecision",

    "Hlt2SingleMuonDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2SingleMuonLowPTDecision",
    "Hlt2SingleMuonHighPTDecision",
    "Hlt2DiMuonDecision",
    "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",
    "Hlt2DiMuonJPsiHighPTDecision",
    "Hlt2DiMuonPsi2SDecision",
    "Hlt2DiMuonDetachedDecision",
    "Hlt2DiMuonDetachedJPsiDecision",
    "Hlt2DiMuonDetachedHeavyDecision",
    "Hlt2DiMuonSoftDecision",
    "Hlt2LowMultDiMuonDecision",
    "Hlt2DiMuonBDecision",
    "Hlt2TopoMu2BodyBBDTDecision",
    "Hlt2TopoMu3BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo2BodySimpleDecision",
    "Hlt2Topo3BodySimpleDecision",
    "Hlt2PassThroughDecision",
    "Hlt2TransparentDecision",
]
TupTmp.L0TISTOS.Verbose = True

TupTmp.Dst.addTool( TupleToolTISTOS, name = "HltTISTOS" )
TupTmp.Dst.ToolList += [ "TupleToolTISTOS/HltTISTOS" ]

TupTmp.Dst.HltTISTOS.TriggerList = [
    "Hlt2RareCharmD02MuMuDecision",
    "Hlt2RareCharmD02PiPiDecision",
    "Hlt2RareCharmD02KPiDecision",
    "Hlt2CharmHadInclDst2PiD02HHXDecision",
    "Hlt2CharmHadMinBiasD02KpiDecision",
    ]
TupTmp.Dst.HltTISTOS.Verbose = True


TupTmp.D0.addTool( TupleToolTISTOS, name = "HltD0TISTOS" )
TupTmp.D0.ToolList += [ "TupleToolTISTOS/HltD0TISTOS" ]

TupTmp.D0.HltD0TISTOS.TriggerList = [
    "Hlt2RareCharmD02MuMuDecision",
    "Hlt2RareCharmD02PiPiDecision",
    "Hlt2RareCharmD02KPiDecision",
    "Hlt2CharmHadInclDst2PiD02HHXDecision",
    "Hlt2CharmHadMinBiasD02KpiDecision",
    ]
TupTmp.D0.HltD0TISTOS.Verbose = True

######################################################################
from Configurables import TupleToolDecayTreeFitter

TupTmp.Dst.addTool( TupleToolDecayTreeFitter, name = "DTF" )
TupTmp.Dst.ToolList += [ "TupleToolDecayTreeFitter/DTF" ]
TupTmp.Dst.addTool( TupTmp.Dst.DTF.clone( "DTF_PV",
                                      Verbose = True,
                                      constrainToOriginVertex = True ) )
TupTmp.Dst.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]


TupTmp.D0.addTool( TupleToolDecayTreeFitter, name = "DTF" )
TupTmp.D0.ToolList += [ "TupleToolDecayTreeFitter/DTF" ]
TupTmp.D0.addTool( TupTmp.D0.DTF.clone( "DTF_PV",
                                      Verbose = True,
                                      constrainToOriginVertex = True ) )
TupTmp.D0.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]

######################################################################

d0_hybrid = TupTmp.D0.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_D0')
d0_hybrid.Variables = {
   'DOCA': "DOCA(0,1)",
   'DOCAMAX' : 'DOCAMAX'
}

all_hybrid = TupTmp.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Variables')
all_hybrid.Variables = {
   'ETA' : 'ETA',
    'RAP' : 'Y',
    'Q' : 'Q',
    
}

Dst_hybrid = TupTmp.Dst.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Dst')
Dst_hybrid.Variables = {
                          "DTF_CTAU"        : "DTF_CTAU( 0, True )"
                        , "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )"
                        , "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )"
                        , "DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )"
                        , "DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )"
                        , "DTF_NDOF"          : "DTF_NDOF(True)"
                        , "DTF_DStar_M"   : "DTF_FUN(M, True)"
                        , "DTF_DStar_P"   : "DTF_FUN(P, True)"
                        , "DTF_DStar_PT"  : "DTF_FUN(PT, True)"
                        , "DTF_DStar_E"   : "DTF_FUN(E, True)"
                        , "DTF_DStar_PX"  : "DTF_FUN(PX, True)"
                        , "DTF_DStar_PY"  : "DTF_FUN(PY, True)"
                        , "DTF_DStar_PZ"  : "DTF_FUN(PZ, True)"
                        , "DTF_Delta_M": "DTF_FUN(M, True) - DTF_FUN(CHILDFUN(M,'D0'==ABSID), True)"
                        , "DTF_D0_M"   : "DTF_FUN(CHILDFUN(M,'D0'==ABSID), True)"
                        , "DTF_D0_P"   : "DTF_FUN(CHILDFUN(P,'D0'==ABSID), True)"
                        , "DTF_D0_PT"  : "DTF_FUN(CHILDFUN(PT,'D0'==ABSID), True)"
                        , "DTF_D0_E"   : "DTF_FUN(CHILDFUN(E,'D0'==ABSID), True)"
                        , "DTF_D0_PX"  : "DTF_FUN(CHILDFUN(PX,'D0'==ABSID), True)"
                        , "DTF_D0_PY"  : "DTF_FUN(CHILDFUN(PY,'D0'==ABSID), True)"
                        , "DTF_D0_PZ"  : "DTF_FUN(CHILDFUN(PZ,'D0'==ABSID), True)"
                        , "DTF_D0_BPVIPCHI2"  : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 'D0'==ABSID), True)"
                        , "DTF_Pis_M"   : "DTF_FUN(CHILDFUN(M,'pi+'==ABSID), True)"
                        , "DTF_Pis_P"   : "DTF_FUN(CHILDFUN(P,'pi+'==ABSID), True)"
                        , "DTF_Pis_PT"  : "DTF_FUN(CHILDFUN(PT,'pi+'==ABSID), True)"
                        , "DTF_Pis_E"   : "DTF_FUN(CHILDFUN(E,'pi+'==ABSID), True)"
                        , "DTF_Pis_PX"  : "DTF_FUN(CHILDFUN(PX,'pi+'==ABSID), True)"
                        , "DTF_Pis_PY"  : "DTF_FUN(CHILDFUN(PY,'pi+'==ABSID), True)"
                        , "DTF_Pis_PZ"  : "DTF_FUN(CHILDFUN(PZ,'pi+'==ABSID), True)"
                        , "DTF_Pis_BPVIPCHI2"  : "DTF_FUN(CHILDFUN(BPVIPCHI2(), 'pi+'==ABSID), True)"
                        , "DTF_CHI2"  : "DTF_CHI2(True)"
                        ,"Delta_M" : "M-M1"
                        ,'DOCAMAX' : 'DOCAMAX'
}

######################################################################

from Configurables import TupleToolSubMass
TupTmp.D0.addTool( TupleToolSubMass )
TupTmp.D0.ToolList += [ "TupleToolSubMass" ]

########################################### MC truth info for simulated samples

from Configurables import TupleToolMCTruth,MCMatchObjP2MCRelator, MCTupleToolKinematic, MCTupleToolHierarchy, MCTupleToolPID

default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

TupTmpMC = TupTmp.clone("DecayTreeTupleForMC") 
TupTmpMC.addTool( TupleToolMCTruth)
TupTmpMC.ToolList += [ "TupleToolMCTruth" ] 
TupTmpMC.TupleToolMCTruth.addTool(MCMatchObjP2MCRelator)
TupTmpMC.TupleToolMCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
TupTmpMC.TupleToolMCTruth.ToolList += [ "MCTupleToolHierarchy" ] 
TupTmpMC.TupleToolMCTruth.ToolList += [ "MCTupleToolKinematic" ] 
TupTmpMC.TupleToolMCTruth.ToolList += [ "MCTupleToolPID" ] 

from Configurables import TupleToolMCBackgroundInfo
TupTmpMC.Dst.ToolList += [ "TupleToolMCBackgroundInfo" ]

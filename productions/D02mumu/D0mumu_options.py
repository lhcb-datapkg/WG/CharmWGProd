from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay

from DecayTupleCreator import TupTmp, TupTmpMC
from DV_Relinfo import getLokiTool, AddLokiCone_had_SINFO, getLokiToolForSubstPID

#####################################################################
#
# Define DecayTreeTuple tuple
#
######################################################################
algs = []
selSeq = []
substSeqPimunu = []
misIDtuple =[]

def setalgs(DataType, whichMC) :

    if DataType == "CL" :
        isMC = False
    elif DataType == "MC" :
        isMC = True

    global TupTmp, TupTmpMC, algs, selSeq, substSeqPimunu,misIDtuple
    if isMC :
        TupTmp = TupTmpMC

    D0mumuTuple = TupTmp.clone("D02mumu_tuple")
    D0mumuTuple.Inputs = ['Phys/DstarD02xxDst2PiD02mumuBox/Particles'] 
    D0mumuTuple.Decay = '[D*(2010)+ -> ^([D0]cc -> ^mu+ ^mu-) ^pi+]CC'
    D0mumuTuple.Branches = {'Dst'  	: '[D*(2010)+ -> ([D0]cc -> mu+ mu-) pi+]CC',
                    		'D0'    	: '[D*(2010)+ -> ^([D0]cc -> mu+ mu-) pi+]CC',
                    		'lab1' 	: '[D*(2010)+ -> ([D0]cc -> ^mu+ mu-) pi+]CC',
                    		'lab2' 	: '[D*(2010)+ -> ([D0]cc -> mu+ ^mu-) pi+]CC',
                    		'pi'    : '[D*(2010)+ -> ([D0]cc -> mu+ mu-) ^pi+]CC'}
    D0mumuTuple.D0.TupleToolSubMass.Substitution       += [ "mu+ => pi+" ]
    D0mumuTuple.D0.TupleToolSubMass.DoubleSubstitution += [ "mu+/mu- => pi+/pi-" ]
    # getLokiTool(D0mumuTuple, "DstarD02xxDst2PiD02mumuBox", isMC)

    D0pipiTuple = TupTmp.clone("D02pipi_tuple")
    D0pipiTuple.Inputs = ['Phys/DstarD02xxDst2PiD02pipiBox/Particles'] 
    D0pipiTuple.Decay = '[D*(2010)+ -> ^([D0]cc -> ^pi+ ^pi-) ^pi+]CC'
    D0pipiTuple.Branches = {'Dst'  : '[D*(2010)+ -> ([D0]cc -> pi+ pi-) pi+]CC',
                    		'D0'    : '[D*(2010)+ -> ^([D0]cc -> pi+ pi-) pi+]CC',
                    		'lab1' : '[D*(2010)+ -> ([D0]cc -> ^pi+ pi-) pi+]CC',
                    		'lab2' : '[D*(2010)+ -> ([D0]cc -> pi+ ^pi-) pi+]CC',
                    		'pi'    : '[D*(2010)+ -> ([D0]cc -> pi+ pi-) ^pi+]CC'}
    D0pipiTuple.D0.TupleToolSubMass.Substitution       += [ "pi+ => mu+" ]
    D0pipiTuple.D0.TupleToolSubMass.DoubleSubstitution += [ "pi+/pi- => mu+/mu-" ]
    # getLokiTool(D0pipiTuple, "DstarD02xxDst2PiD02pipiBox", isMC)

    D0kpiTuple = TupTmp.clone("D02Kpi_tuple")
    D0kpiTuple.Inputs = ['Phys/DstarD02xxDst2PiD02KpiBox/Particles'] 
    D0kpiTuple.Decay = '[D*(2010)+ -> ^([D0]cc -> ^K- ^pi+) ^pi+]CC'
    D0kpiTuple.Branches = {'Dst'  : '[D*(2010)+ -> ([D0]cc -> K- pi+) pi+]CC',
                    		'D0'    : '[D*(2010)+ -> ^([D0]cc -> K- pi+) pi+]CC',
                    		'lab1' : '[D*(2010)+ -> ([D0]cc -> ^K- pi+) pi+]CC',
                    		'lab2' : '[D*(2010)+ -> ([D0]cc -> K- ^pi+) pi+]CC',
                    		'pi': '[D*(2010)+ -> ([D0]cc -> K- pi+) ^pi+]CC'}
    D0kpiTuple.D0.TupleToolSubMass.DoubleSubstitution += [ "K-/pi+ => mu-/mu+" ]
    # getLokiTool(D0kpiTuple, "DstarD02xxDst2PiD02KpiBox", isMC)

    D0pimunuTuple = TupTmp.clone("D02pimunu_tuple")
    pimunuLine =  'D2pimuLine'
    D0pimunuTuple.Inputs = ['Phys/'+pimunuLine+'/Particles'] 
    D0pimunuTuple.Decay = '[D*(2010)+ -> ^([D0]CC -> ^pi- ^mu+) ^pi+]CC'
    D0pimunuTuple.Branches = {'Dst'   : '[D*(2010)+ -> ([D0]CC -> pi- mu+) pi+]CC',
                            'D0'        : '[D*(2010)+ -> ^([D0]CC -> pi- mu+) pi+]CC',
                            'lab1'  : '[D*(2010)+ -> ([D0]CC -> ^pi- mu+) pi+]CC',
                            'lab2'  : '[D*(2010)+ -> ([D0]CC -> pi- ^mu+) pi+]CC',
                            'pi'    : '[D*(2010)+ -> ([D0]CC -> pi- mu+) ^pi+]CC'}
    D0pimunuTuple.D0.TupleToolSubMass.Substitution       += [ "pi- => mu-" ]

    algs += [D0mumuTuple,D0pipiTuple,D0kpiTuple,D0pimunuTuple] 

######################################################################
#
# Define DecayTreeTuple tuple for Kpi with substituted mass
#
######################################################################
    from Configurables import SubstitutePID
    from PhysSelPython.Wrappers import Selection, AutomaticData, SelectionSequence

    subs_kpi   = SubstitutePID(
            'PIDsubstKpi',
            Code="DECTREE('[ D*(2010)+ -> (D0 -> K- pi+ ) pi+ ]CC')",
            # note that SubstitutePID can't handle automatic CC
            Substitutions={
                'Charm -> (D0 -> ^K- X+ ) Meson': 'mu-',
                'Charm -> (D0 -> ^pi+ X-) Meson': 'mu+',
                'Charm -> (D~0 -> ^K+ X- ) Meson': 'mu+',
                'Charm -> (D~0 -> ^pi- X+ ) Meson': 'mu-'}

    )
    ParticlesKpi_fromStripping = AutomaticData('Phys/DstarD02xxDst2PiD02KpiBox/Particles')

    flter = getLokiToolForSubstPID("DstarD02xxDst2PiD02KpiBox", isMC)
    SaveRelInfo =  Selection("InfoWriter_Sel", Algorithm = flter, RequiredSelections = [ParticlesKpi_fromStripping] )
    MySubtsPID_Kpi = Selection('PIDsubstKpi_sel',Algorithm=subs_kpi,RequiredSelections=[SaveRelInfo])
    selSeq = SelectionSequence('SelSeq', TopSelection=MySubtsPID_Kpi)

    D0kpiTuple_misID = TupTmp.clone("D0KpiTuple_misID")
    D0kpiTuple_misID.Inputs = [selSeq.outputLocation()]
    D0kpiTuple_misID.Decay = '[D*(2010)+ -> ^([D0]CC -> ^mu+ ^mu-) ^pi+]CC'
    D0kpiTuple_misID.Branches = {'Dst'      : '[D*(2010)+ -> ([D0]CC -> mu+ mu-) pi+]CC',
                            'D0'        : '[D*(2010)+ -> ^([D0]CC -> mu+ mu-) pi+]CC',
                            'lab1'  : '[D*(2010)+ -> ([D0]CC -> ^mu+ mu-) pi+]CC',
                            'lab2'  : '[D*(2010)+ -> ([D0]CC -> mu+ ^mu-) pi+]CC',
                            'pi'    : '[D*(2010)+ -> ([D0]CC -> mu+ mu-) ^pi+]CC'}
    AddLokiCone_had_SINFO(D0kpiTuple_misID)

    misIDtuple +=[D0kpiTuple_misID]

######################################################################
#
# Define DecayTreeTuple tuple for Pimunu with substituted mass
#
######################################################################
    subs_pimunu  = SubstitutePID(
            'PIDsubstPimunu',
            Code="DECTREE('[ D*(2010)+ -> (D0 -> pi- mu+ ) pi+ ]CC')", 
            # note that SubstitutePID can't handle automatic CC
            Substitutions={
                'Charm -> (D0 -> ^pi- X+ ) Meson': 'mu-',
                'Charm -> (D~0 -> ^pi+ X- ) Meson': 'mu+'}

    )
    ParticlesPimunu_fromStripping = AutomaticData('Phys/'+pimunuLine+'/Particles')
    MySubtsPID_Pimunu = Selection('PIDsubstPimunu_sel',Algorithm=subs_pimunu,RequiredSelections=[ParticlesPimunu_fromStripping])
    substSeqPimunu = SelectionSequence('substSeqPimunu', TopSelection=MySubtsPID_Pimunu)

    D02PimunuTuple_misID = TupTmp.clone("D02PimunuTuple_misID")
    D02PimunuTuple_misID.Inputs = [substSeqPimunu.outputLocation()] 
    D02PimunuTuple_misID.Decay = '[D*(2010)+ -> ^([D0]CC -> ^mu+ ^mu-) ^pi+]CC'
    D02PimunuTuple_misID.Branches = {'Dst'      : '[D*(2010)+ -> ([D0]CC -> mu+ mu-) pi+]CC',
                            'D0'        : '[D*(2010)+ -> ^([D0]CC -> mu+ mu-) pi+]CC',
                            'lab1'  : '[D*(2010)+ -> ([D0]CC -> ^mu+ mu-) pi+]CC',
                            'lab2'  : '[D*(2010)+ -> ([D0]CC -> mu+ ^mu-) pi+]CC',
                            'pi'    : '[D*(2010)+ -> ([D0]CC -> mu+ mu-) ^pi+]CC'}

    misIDtuple +=[D02PimunuTuple_misID]


##################################################################################################

    #Add MCtuple

##################################################################################################
    if not isMC:
        return

    print "Adding MCDecayTreeTuple"

    from Configurables import MCDecayTreeTuple, MCTupleToolKinematic
    MCTupTmp = MCDecayTreeTuple()
    
    if len(whichMC) is 2:
        MCTupTmp.Decay = '[D*(2010)+ => ^([D0]CC -> ^' + whichMC[0] + ' ^' + whichMC[1] + ') ^pi+]CC'
        MCTupTmp.Branches = {'Dst'      : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] +  whichMC[1] + ') pi+]CC',
                                 'D0'        : '[D*(2010)+ -> ^([D0]CC -> ' + whichMC[0] +  whichMC[1] + ') pi+]CC',
                                 'lab1'  : '[D*(2010)+ -> ([D0]CC -> ^' + whichMC[0] +  whichMC[1] +') pi+]CC',
                                 'lab2'  : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] + ' ^' +  whichMC[1] +') pi+]CC',
                                 'pi'    : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] + whichMC[1] + ') ^pi+]CC'}
    elif len(whichMC) is 3:
        MCTupTmp.Decay = '[D*(2010)+ => ^([D0]CC -> ^' + whichMC[0] + ' ^' + whichMC[1] + ' ^' + whichMC[2] +') ^pi+]CC'
        MCTupTmp.Branches = {'Dst'      : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] +  whichMC[1] + whichMC[2] + ') pi+]CC',
                                 'D0'        : '[D*(2010)+ -> ^([D0]CC -> ' + whichMC[0] +  whichMC[1] + whichMC[2] + ') pi+]CC',
                                 'lab1'  : '[D*(2010)+ -> ([D0]CC -> ^' + whichMC[0] +  whichMC[1] + whichMC[2] +') pi+]CC',
                                 'lab2'  : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] + ' ^' +  whichMC[1] + whichMC[2] +') pi+]CC',
                                 'nu'    : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] + whichMC[1] + ' ^' + whichMC[2] + ') pi+]CC',
                                 'pi'    : '[D*(2010)+ -> ([D0]CC -> ' + whichMC[0] + whichMC[1] + whichMC[2] + ') ^pi+]CC'}
    
    MCTupTmp.ToolList += ['MCTupleToolHierarchy']
    MCTupTmp.ToolList += ['MCTupleToolKinematic']
    # MCTupTmp.ToolList += ['MCTupleToolReconstructed']
    MCTupTmp.ToolList += ['MCTupleToolPID']
    
    algs += [MCTupTmp]
    print algs

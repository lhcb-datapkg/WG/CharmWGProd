import sys, os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/D02mumu')

import D0mumu_options as opts
from DV_Config import ConfigDaVinci

#######################To set#######################
DataType = 'MC' #CL or MC
DataYear = '17'
isTest = False
Mag = 'MU'

# whichMC =['pi+', 'pi-'] # ['mu+', 'mu-'], ['pi+', 'pi-'], ['K-, 'pi+']
whichMC =['pi-', 'mu+', 'nu_mu']
####################################################
 
opts.setalgs(DataType,whichMC)
ConfigDaVinci("CHARM_MC_D02PIMUNU_2018_MD.root",DataType, DataYear, opts.algs, opts.misIDtuple,isTest=isTest, Mag=Mag )

#NOTE: There is an application in the stripping of StdLooseXXX to StdAllNoPIDsXXX
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import  DaVinci, DecayTreeTuple, TupleToolTrigger
from Configurables import  GaudiSequencer
from Configurables import  DecayTreeTuple, MCDecayTreeTuple, CheckPV
from Configurables import TupleToolDecayTreeFitter
from Configurables import  CombineParticles, FilterDesktop,  OfflineVertexFitter, TupleToolDecay, LoKi__Hybrid__TupleTool, TupleToolPid, TupleToolTrackInfo, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolTrigger, TupleToolEventInfo, TupleToolGeometry, TupleToolTISTOS, TupleToolPid, TupleToolRecoStats, TupleToolTrackPosition,TupleToolBremInfo
from Configurables import LoKi__Hybrid__EvtTupleTool as MyTool 
from Configurables import DecayTreeTuple, LoKi__Hybrid__TupleTool, TupleToolDecay, TupleToolTrigger, TupleToolTISTOS, TupleToolTrackPosition
from Configurables import LoKi__Hybrid__TupleTool
from Gaudi.Configuration import *
from Configurables import SelDSTWriter, DaVinci
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand,AutomaticData
from LoKiPhys.decorators import *
from LoKiPhysMC.decorators import *
from LoKiPhysMC.functions import *
# from Configurables import  ProcStatusCheck
# filterBadEvents =  ProcStatusCheck()



# Remove the microbias and beam gas etc events before doing the tagging step
# regexp = "HLT_PASS_RE('Hlt1(?!ODIN)(?!L0)(?!Lumi)(?!Tell1)(?!MB)(?!NZS)(?!Velo)(?!BeamGas)(?!Incident).*Decision')"
# from Configurables import LoKi__HDRFilter
# filterHLT = LoKi__HDRFilter("FilterHLT",Code = regexp )

# #userAlgos+=[DstDPiMuSelSeq,DstDPiESelSeq]

#tuple it
tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolTrigger",
                 #"TupleToolPid",
                 "TupleToolANNPID",
                 #"TupleToolExtraMu"
                 #"TupleToolMCTruth",
                 "TupleToolMCBackgroundInfo",
                 #"LoKi::Hybrid::MCTupleTool/LoKi_Photos",
                 #"MCTupleToolHierarchy"
                 ]

mctupletoollist = ["MCTupleToolHierarchy",
                   "MCTupleToolReconstructed",
                   "MCTupleToolKinematic",
                   "TupleToolMCTruth",
                   "TupleToolMCBackgroundInfo",
                   #"MCTupleToolAngles"
                   "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
                   ]

###HERE!!
#kaon tuples
DtoKPiPiMuNu = DecayTreeTuple("DtoKPiPiMuNu")
DtoKPiPiMuNu.Inputs = ['CharmCompleteEvent/Phys/D2KpipimuLine/Particles' ]
DtoKPiPiMuNu.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi- ^mu+) ^pi+ ]CC'
DtoKPiPiMuNu.Branches = {
    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- pi+ pi- mu+) pi+ ]CC)',
    'D'     : '[ D*(2010)+ -> ^(D0 -> K- pi+ pi- mu+) pi+ ]CC',
    'Km'    : '[ D*(2010)+ -> (D0 -> ^K- pi+ pi- mu+) pi+ ]CC',
    'Ps'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- mu+) ^pi+ ]CC',
    'Mu'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- ^mu+) pi+ ]CC',
    'pip'    : '[ D*(2010)+ -> (D0 -> K- ^pi+ pi- mu+) pi+ ]CC',
    'pim'    : '[ D*(2010)+ -> (D0 -> K- pi+ ^pi- mu+) pi+ ]CC'
    }

DtoKPiPiMuNuKMuSS = DecayTreeTuple("DtoKPiPiMuNuKMuSS")
DtoKPiPiMuNuKMuSS.Inputs = ['CharmCompleteEvent/Phys/D2Kpipimu_KLepSSLine/Particles' ]
DtoKPiPiMuNuKMuSS.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi+ ^mu-) ^pi+ ]CC'
DtoKPiPiMuNuKMuSS.Branches = {
    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- pi+ pi+ mu-) pi+ ]CC)',
    'D'     : '[ D*(2010)+ -> ^(D0 -> K- pi+ pi+ mu-) pi+ ]CC',
    'Km'    : '[ D*(2010)+ -> (D0 -> ^K- pi+ pi+ mu-) pi+ ]CC',
    'Ps'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi+ mu-) ^pi+ ]CC',
    'Mu'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi+ ^mu-) pi+ ]CC',
    'pip'    : '[ D*(2010)+ -> (D0 -> K- ^pi+ pi+ mu-) pi+ ]CC',
    'pim'    : '[ D*(2010)+ -> (D0 -> K- pi+ ^pi+ mu-) pi+ ]CC'
    }

DtoKPiPiMuNuSS = DecayTreeTuple("DtoKPiPiMuNuSS")
DtoKPiPiMuNuSS.Inputs = ['CharmCompleteEvent/Phys/D2Kpipimu_SSLine/Particles' ]
DtoKPiPiMuNuSS.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi- ^mu-) ^pi+ ]CC'
DtoKPiPiMuNuSS.Branches = {
    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- pi+ pi- mu-) pi+ ]CC)',
    'D'     : '[ D*(2010)+ -> ^(D0 -> K- pi+ pi- mu-) pi+ ]CC',
    'Km'    : '[ D*(2010)+ -> (D0 -> ^K- pi+ pi- mu-) pi+ ]CC',
    'Ps'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- mu-) ^pi+ ]CC',
    'Mu'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- ^mu-) pi+ ]CC',
    'pip'    : '[ D*(2010)+ -> (D0 -> K- ^pi+ pi- mu-) pi+ ]CC',
    'pim'    : '[ D*(2010)+ -> (D0 -> K- pi+ ^pi- mu-) pi+ ]CC'
    }

DtoKPiPiENu = DecayTreeTuple("DtoKPiPiENu")
DtoKPiPiENu.Inputs = ['CharmCompleteEvent/Phys/D2KpipiELine/Particles' ]
DtoKPiPiENu.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi- ^e+) ^pi+ ]CC'
DtoKPiPiENu.Branches = {
    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- pi+ pi- e+) pi+ ]CC)',
    'D'     : '[ D*(2010)+ -> ^(D0 -> K- pi+ pi- e+) pi+ ]CC',
    'Km'    : '[ D*(2010)+ -> (D0 -> ^K- pi+ pi- e+) pi+ ]CC',
    'Ps'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- e+) ^pi+ ]CC',
    'E'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- ^e+) pi+ ]CC',
    'pip'    : '[ D*(2010)+ -> (D0 -> K- ^pi+ pi- e+) pi+ ]CC',
    'pim'    : '[ D*(2010)+ -> (D0 -> K- pi+ ^pi- e+) pi+ ]CC'
    }

DtoKPiPiENuKESS = DecayTreeTuple("DtoKPiPiENuKESS")
DtoKPiPiENuKESS.Inputs = ['CharmCompleteEvent/Phys/D2KpipiE_KLepSSLine/Particles' ]
DtoKPiPiENuKESS.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi+ ^e-) ^pi+ ]CC'
DtoKPiPiENuKESS.Branches = {
    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- pi+ pi+ e-) pi+ ]CC)',
    'D'     : '[ D*(2010)+ -> ^(D0 -> K- pi+ pi+ e-) pi+ ]CC',
    'Km'    : '[ D*(2010)+ -> (D0 -> ^K- pi+ pi+ e-) pi+ ]CC',
    'Ps'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi+ e-) ^pi+ ]CC',
    'E'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi+ ^e-) pi+ ]CC',
    'pip'    : '[ D*(2010)+ -> (D0 -> K- ^pi+ pi+ e-) pi+ ]CC',
    'pim'    : '[ D*(2010)+ -> (D0 -> K- pi+ ^pi+ e-) pi+ ]CC'
    }

DtoKPiPiENuSS = DecayTreeTuple("DtoKPiPiENuSS")
DtoKPiPiENuSS.Inputs = ['CharmCompleteEvent/Phys/D2KpipiE_SSLine/Particles' ]
DtoKPiPiENuSS.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi- ^e-) ^pi+ ]CC'
DtoKPiPiENuSS.Branches = {
    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- pi+ pi- e-) pi+ ]CC)',
    'D'     : '[ D*(2010)+ -> ^(D0 -> K- pi+ pi- e-) pi+ ]CC',
    'Km'    : '[ D*(2010)+ -> (D0 -> ^K- pi+ pi- e-) pi+ ]CC',
    'Ps'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- e-) ^pi+ ]CC',
    'E'    : '[ D*(2010)+ -> (D0 -> K- pi+ pi- ^e-) pi+ ]CC',
    'pip'    : '[ D*(2010)+ -> (D0 -> K- ^pi+ pi- e-) pi+ ]CC',
    'pim'    : '[ D*(2010)+ -> (D0 -> K- pi+ ^pi- e-) pi+ ]CC'
    }

#MC DTT
#MC_DtoKMuNu = MCDecayTreeTuple("MC_DtoKMuNu")
#MC_DtoKMuNu.Inputs = ['/Event/AllStreams/Phys/D2KmuLine/Particles' ]
#MC_DtoKMuNu.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^mu+ ^nu_mu) ^pi+ ]CC'
#MC_DtoKMuNu.Branches = {
#    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- mu+ nu_mu) pi+ ]CC)',
#    'D'     : '[ D*(2010)+ -> ^(D0 -> K- mu+ nu_mu) pi+ ]CC',
#    'K'    : '[ D*(2010)+ -> (D0 -> ^K- mu+ nu_mu) pi+ ]CC',
#    'Ps'    : '[ D*(2010)+ -> (D0 -> K- mu+ nu_mu) ^pi+ ]CC',
#    'Mu'    : '[ D*(2010)+ -> (D0 -> K- ^mu+ nu_mu) pi+ ]CC',
#    'NuMu'    : '[ D*(2010)+ -> (D0 -> K- mu+ ^nu_mu) pi+ ]CC'
#    }

##same sign
#DtoKMuNuSS = DecayTreeTuple("DtoKMuNuSS")
#DtoKMuNuSS.Inputs = ['Event/AllStreams/Phys/D2KmuSSLine/Particles' ]
#DtoKMuNuSS.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^mu-) ^pi+ ]CC'
#DtoKMuNuSS.Branches = {
#    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- mu-) pi+ ]CC)',
#    'D'     : '[ D*(2010)+ -> ^(D0 -> K- mu-) pi+ ]CC',
#    'K'    : '[ D*(2010)+ -> (D0 -> ^K- mu-) pi+ ]CC',
#    'Ps'    : '[ D*(2010)+ -> (D0 -> K- mu-) ^pi+ ]CC',
#    'Mu'    : '[ D*(2010)+ -> (D0 -> K- ^mu-) pi+ ]CC'
#    }

#DtoKeNuSS = DecayTreeTuple("DtoKeNuSS")
#DtoKeNuSS.Inputs = ['Event/AllStreams/Phys/D2KESSLine/Particles']
                     
#DtoKeNuSS.Decay = '[ D*(2010)+ -> ^(D0 -> ^K- ^e-) ^pi+ ]CC'
#DtoKeNuSS.Branches = {
#    'Dstar' : '^([ D*(2010)+ -> (D0 -> K- e-) pi+ ]CC)',
#    'D'     : '[ D*(2010)+ -> ^(D0 -> K- e-) pi+ ]CC',
#    'K'    : '[ D*(2010)+ -> (D0 -> ^K- e-) pi+ ]CC',
#    'Ps'    : '[ D*(2010)+ -> (D0 -> K- e-) ^pi+ ]CC',
#    'electron' : '[ D*(2010)+ -> (D0 -> K- ^e-) pi+ ]CC'
#    }


###goood up to here
#add to the tuples the correct tools
from Configurables import TupleToolTISTOS, TupleToolNeutrinoReco,TupleToolKinematic
from DecayTreeTuple.Configuration import *
tuplesMu = [
        DtoKPiPiMuNu,
        DtoKPiPiMuNuKMuSS,
        DtoKPiPiMuNuSS,
          ]
tuplesE = [
        DtoKPiPiENu,
        DtoKPiPiENuKESS,
        DtoKPiPiENuSS,
          ]
tuples = tuplesMu
tuples += tuplesE

trig_list = ['L0HadronDecision',
             'L0HadronNoSPDDecision',
             'L0PhotonNoSPDDecision',
             'L0PhotonHiDecision',
             'L0PhotonDecision',
             'L0ElectronDecision',
             'L0ElectronNoSPDDecision',
             'L0MuonDecision',
             'L0DiMuonDecision',
             'Hlt1TrackMVADecision',
             'Hlt1TwoTrackMVADecision',
             'Hlt1TrackMuonDecision',
             'Hlt1TrackMVAMuonDecision',
             'Hlt1GlobalDecision',
             'Hlt1TrackAllL0Decision',
             'Hlt2SingleMuonDecision',
             'Hlt2TopoMu4BodyDecision',
             'Hlt2TopoMu3BodyDecision',
             'Hlt2TopoMu2BodyDecision',
             'Hlt2CharmHadInclDst2PiD02HHXBDTDecision',
             'Hlt2CharmHadInclSigc2PiLc2HHXBDTDecision',
             'Hlt2RareCharmD02{KPi,KK,PiPi}MuMuDecision',
             'Hlt2RareCharmD02{KPi,KK,PiPi}eeDecision',
             ]

from Configurables import TupleToolTrackIsolation
for tuple in tuples:
    tuple.ToolList+=tupletoollist
    #rest
    tuple.addTool(TupleToolPid, name="TupleToolPid")
    tuple.TupleToolPid.Verbose=True
    tuple.addTool(TupleToolKinematic,name="TupleToolKinematic")
    tuple.TupleToolKinematic.Verbose=True
    tuple.addTool(TupleToolDecay,name="Dstar")
    tuple.Dstar.ToolList+=["TupleToolTISTOS/ttTTdstar"]
    tuple.Dstar.addTool(TupleToolTISTOS,name="ttTTdstar")
    tuple.Dstar.ttTTdstar.VerboseL0=True
    tuple.Dstar.ttTTdstar.VerboseHlt1=True
    tuple.Dstar.ttTTdstar.VerboseHlt2=True
    tuple.Dstar.ttTTdstar.TriggerList=trig_list

    tuple.Dstar.ToolList+=["TupleToolTrackIsolation"]
    tuple.Dstar.addTool( TupleToolTrackIsolation, name="TupleToolTrackIsolation" )
    tuple.Dstar.TupleToolTrackIsolation.Verbose      = True
    tuple.Dstar.TupleToolTrackIsolation.MinConeAngle = 0.9
    tuple.Dstar.TupleToolTrackIsolation.MaxConeAngle = 1.6
    tuple.Dstar.TupleToolTrackIsolation.StepSize     = 0.1
    
    #TISTOS
    #D
    tuple.addTool(TupleToolDecay,name="D")
    tuple.D.ToolList+=["TupleToolNeutrinoReco/ttnr"]
    tuple.D.addTool(TupleToolNeutrinoReco,name="ttnr")
    tuple.D.ttnr.Verbose=True
    tuple.D.ttnr.MotherMass= 1864.84#MeV
    tuple.D.ToolList+=["TupleToolTISTOS/ttTTD"]
    tuple.D.addTool(TupleToolTISTOS,name="ttTTD")
    tuple.D.ttTTD.VerboseL0=True
    tuple.D.ttTTD.VerboseHlt1=True
    tuple.D.ttTTD.VerboseHlt2=True
    tuple.D.ttTTD.TriggerList=trig_list
    #K
    tuple.addTool(TupleToolDecay,name='Km')
    tuple.Km.ToolList+=['TupleToolTISTOS/ttTTKm']
    tuple.Km.addTool(TupleToolTISTOS,name='ttTTKm')
    tuple.Km.ttTTKm.VerboseL0=True
    tuple.Km.ttTTKm.VerboseHlt1=True
    tuple.Km.ttTTKm.VerboseHlt2=True
    tuple.Km.ttTTKm.TriggerList=trig_list
    tuple.addTool(TupleToolDecay,name='pip')
    tuple.pip.ToolList+=['TupleToolTISTOS/ttTTpip']
    tuple.pip.addTool(TupleToolTISTOS,name='ttTTpip')
    tuple.pip.ttTTpip.VerboseL0=True
    tuple.pip.ttTTpip.VerboseHlt1=True
    tuple.pip.ttTTpip.VerboseHlt2=True
    tuple.pip.ttTTpip.TriggerList=trig_list
    tuple.addTool(TupleToolDecay,name='pim')
    tuple.pim.ToolList+=['TupleToolTISTOS/ttTTpim']
    tuple.pim.addTool(TupleToolTISTOS,name='ttTTpim')
    tuple.pim.ttTTpim.VerboseL0=True
    tuple.pim.ttTTpim.VerboseHlt1=True
    tuple.pim.ttTTpim.VerboseHlt2=True
    tuple.pim.ttTTpim.TriggerList=trig_list
    
    LoKi_D = tuple.D.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D")

    LoKi_D.Variables={'KEY' : 'KEY',
                      "CORR_M" : "BPVCORRM",
                      "MAXDOCA" : "DOCAMAX",
                      "MINIPCHI2" : "MIPCHI2DV(PRIMARY)",
                      "BPVDIRA"   : "BPVDIRA"
                      , "BPVVDCHI2" : "BPVVDCHI2"
                      , "BPVIPCHI2" : "BPVIPCHI2()"
                      , "BPVVDZ"    : "BPVVDZ"
                      , "VFASPF"    : "VFASPF(VCHI2/VDOF)"
                      }
    
    #try neutrino reconstruction
        #dont use DTF to see if it doesn't mess up the pv position
    #tuple.D.ToolList+=[ "TupleToolDecayTreeFitter/VFit" ]

    #tuple.D.addTool(TupleToolDecayTreeFitter("VFit"))
    #tuple.D.VFit.Verbose = True
    #tuple.D.VFit.constrainToOriginVertex =False

for tuple in tuplesMu:
    #extra muon
    tuple.addTool(TupleToolDecay,name="Mu")
    # tuple.Mu.ToolList+=["TupleToolExtraMu/extramu"]
    # tuple.Mu.addTool(TupleToolExtraMu,name="extramu")
    # tuple.Mu.extramu.Verbose=True
    #tuple.Mu.extramu.OutputLevel = DEBUG
    
    tuple.Mu.ToolList+=["TupleToolTISTOS/ttTTmu"]
    tuple.Mu.addTool(TupleToolTISTOS,name="ttTTmu")
    tuple.Mu.ttTTmu.VerboseL0=True
    tuple.Mu.ttTTmu.VerboseHlt1=True
    tuple.Mu.ttTTmu.VerboseHlt2=True
    tuple.Mu.ttTTmu.TriggerList=trig_list

for tuple in tuplesE:
    tuple.addTool(TupleToolDecay,name="E")
    # tuple.Mu.ToolList+=["TupleToolExtraMu/extramu"]
    # tuple.Mu.addTool(TupleToolExtraMu,name="extramu")
    # tuple.Mu.extramu.Verbose=True
    #tuple.Mu.extramu.OutputLevel = DEBUG
    
    tuple.E.ToolList+=["TupleToolTISTOS/ttTTe"]
    tuple.E.addTool(TupleToolTISTOS,name="ttTTe")
    tuple.E.ttTTe.VerboseL0=True
    tuple.E.ttTTe.VerboseHlt1=True
    tuple.E.ttTTe.VerboseHlt2=True
    tuple.E.ttTTe.TriggerList=trig_list
    tuple.E.ToolList+=["TupleToolBremInfo/ttBe"]
    tuple.E.addTool(TupleToolBremInfo,name="ttBe")
    

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#MessageSvc().OutputLevel = DEBUG #get a lot of info

#from Configurables import LoKi__HDRFilter as StripFilter
#line = 'b2D0MuXB2DMuNuXLine' #single tagged line


from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

from Configurables import FilterDesktop

DaVinci().PrintFreq = 1000
#DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().TupleFile = "D2KPIPIMUNU_NTUPLE.ROOT"
DaVinci().EvtMax = -1
#DaVinci().EventPreFilters = [ filterHLT ]
#DaVinci().VerboseMessages = True

#DaVinci().appendToMainSequence( [StripFilter( 'StripPassFilter', Code="HLT_PASS('Stripping"+line+"Decision')", Location="/Event/Strip/Phys/DecReports" )])

DaVinci().appendToMainSequence( tuples )

#CondDB.LatestGlobalTagsByDataType ="2015"
#DaVinci().DataType = "2015"
DaVinci().InputType = 'DST'
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation

# Conditions from: https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
#DaVinci().CondDBtag = "sim-20161124-2-vc-md100"
#DaVinci().DDDBtag = "dddb-20150724"


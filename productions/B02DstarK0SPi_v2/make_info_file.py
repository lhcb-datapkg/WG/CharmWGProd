#!/usr/bin/env python
import argparse
import json
from os.path import dirname, join

'''
A script to produce info.json for specific tuple production.
When a tuple production is needed, the script is called to generate a new info.json accordingly.
A merge request is submitted with no change of the tuple production code but the info.json.

For example, when submitting a tuple production for data, prepare info.json as
    $ python make_info_file.py
If only 2017 and 2018 data, mag up is needed
    $ python make_info_file.py --years 2017 2018 --polarities MagUp

If only 2012 MC of event type 11166170 is needed
    $ python make_info_file.py --years 2012 --evtIds 11166170
'''

tcks = {
    #'2011': '0x40760037',
    '2012': '0x409f0045',
    '2015': '0x411400a2',
    '2016': '0x6139160F',
    '2017': '0x62661709',
    '2018': '0x617d18a4',
}
strip_data = {
    '2011': 'Stripping21r1',
    '2012': 'Stripping21',
    '2015': 'Stripping24r1',
    '2016': 'Stripping28r1',
    '2017': 'Stripping29r2',
    '2018': 'Stripping34',
}
strip_mc = {
    #'2011': 'Stripping20r1NoPrescalingFlagged',
    '2012': 'Stripping21NoPrescalingFlagged',
    '2015': 'Turbo02/Stripping24r1NoPrescalingFlagged',
    '2016': 'Turbo03a/Stripping28r1NoPrescalingFlagged',
    '2017': 'Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged',
    '2018': 'Turbo05-WithTurcal/Stripping34NoPrescalingFlagged',
}
Beam = {'2011': '3500', '2012': '4000', '2015': '6500', '2016': '6500', '2017': '6500', '2018': '6500'}
Nu =   {'2011': '2', '2012': '2.5', '2015': '1.6-25ns', '2016': '1.6-25ns', '2017': '1.6-25ns', '2018': '1.6-25ns'}
Reco = {'2011': '14', '2012': '14', '2015': '15a', '2016': '16', '2017': '17', '2018': '18'}
RecoMC = {'2011': '14c', '2012': '14c', '2015': '15a', '2016': '16', '2017': '17', '2018': '18'}
mcsamples = [13166160,11166170]
bk_mc_path = '/MC/{0}/Beam{1}GeV-{0}-{2}-Nu{3}-Pythia8/Sim08h/Digi13/' \
             'Trig{4}/Reco{5}/{6}/{7}/ALLSTREAMS.DST'#.format(date,beam[date],mag,nu,tck,reco,trigstrip,evtid) # for later productions Digi13 may be removed from path and Sim should be 09
bk_data_path = '/LHCb/Collision{0}/Beam{1}GeV-VeloClosed-{2}/Real Data/Reco{3}/{4}/90000000/BHADRON.MDST'#.format(year%100,beam,mag,reco,strip)

all_years = [2011,2012]+range(2015,2019) # missing 2011
all_polarities = ['MagDown', 'MagUp']
dv_version = 'v44r7'

parser = argparse.ArgumentParser()
parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
parser.add_argument('--evtIds', type=int, nargs='+', choices=mcsamples, default=[])
parser.add_argument('--force-full-lfn', action='store_false', help='Force the merge request testing to use a full LFN')
args = parser.parse_args()

def main():
    results = {}
    for year in args.years:
        y = str(year)
        for m in args.polarities:
            if len(args.evtIds):
                for evtId in args.evtIds:
                    key = '_'.join([y,m,str(evtId)])
                    assert key not in results, 'Duplicate keys are not possible'
                    results[key] = {
                        "options": ["data_type_{}.py".format(y), "simulation.py", "main_options.py"],
                        "bookkeeping_path": bk_mc_path.format(y,Beam[y],m,Nu[y],tcks[y],RecoMC[y],strip_mc[y],evtId),
                        "dq_flag": "OK",
                        "application": "DaVinci",
                        "application_version": dv_version,
                        "output_type": "B02DSTARK0SPI_{}_DVNTUPLE.ROOT".format(evtId)
                    }
            else:
                key = '_'.join([y,m])
                assert key not in results, 'Duplicate keys are not possible'
                results[key] = {
                    "options": ["data_type_{}.py".format(y), "main_options.py"],
                    "bookkeeping_path": bk_data_path.format(year%100,Beam[y],m,Reco[y],strip_data[y]),
                    "dq_flag": "OK",
                    "application": "DaVinci",
                    "application_version": dv_version,
                    "output_type": "B02DSTARK0SPI_DVNTUPLE.ROOT"
                }
    print(results)
        # for key in results.keys():
        #     if args.force_full_lfn:
        #         results[key]['n_events'] = -1
        #         results[key]['n_lfns'] = 2
    with open(join(dirname(__file__), 'info.json'), 'wt') as fp:
        json.dump(results, fp, indent=4, sort_keys=True)

    return

if __name__ == '__main__':
    main()

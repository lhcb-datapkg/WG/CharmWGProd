All options are tested with

DaVinci/v42r6p1 (Run1 script)

DaVinci/v42r7p3 (Run2 2015-2017 scripts)

DaVinci/v44r6 (Run2 2018 scripts) [no Momentum Scaling]
DaVinci/v44r9 (Run2 2018 scripts)

The scripts produce ntuples for D -> V0 H like decays, the channels are:
D -> KS0LL Pi  	        
D -> KS0DD Pi  		
D -> KS0LL K  	        
D -> KS0DD K		
D -> Phi(->KmKp) Pi	
D_s -> Phi(->KmKp) Pi	

Momentum scaling correction has been is applyied in Run 2 options files (Dplus_DTFonlyMS_* variables).

There are eight set of options:

1) for Run 1 (years 2011 and 2012) data, it produces all the six decays, here D_s -> PhiPi is inside D -> PhiPi line.

2) for 2015 (Run 2) data, it produces all the six decays.

3) for 2016 (Run 2) data, it produces all the channels exept D_s -> PhiPi .

4) for 2016 (Run 2) data, it produces D_s -> PhiPi channel.

5) for 2017 (Run 2) data, it produces all the channels exept D_s -> PhiPi .

6) for 2017 (Run 2) data, it produces D_s -> PhiPi channel.

7) for 2018 (Run 2) data, it produces all the channels exept D_s -> PhiPi .

8) for 2018 (Run 2) data, it produces D_s -> PhiPi channel.
 

---------- How to run : ----------

1) To be run with s20 and s20r1 data.
    DataType201*.py D2KS0H_D2PhiPi_MS_201*.py

2) To be run with Turbo02 data.
    D2KS0H_D2PhiPi_MS_2015.py
   
3) To be run with Turbo03a data, stream : CHARMCHARGED.
    D2KS0H_D2PhiPi_MS_2016.py
   
4) To be run with Turbo03a data, stream : CHARMSPECPARKED.
   (This includes also CHARMSPECPRESCALED so it is 100% + 10% of the sample -> take only the 100%)
    Ds2PhiPi_MS_2016.py

5) To be run with Turbo04 data, stream : CHARMCHARGED.
    D2KS0H_D2PhiPi_MS_2017.py
   
6) To be run with Turbo04 data, stream : CHARMSPEC.
    Ds2PhiPi_MS_2017.py
  
7) To be run with Turbo05 data, stream : CHARMCHARGED.
    D2KS0H_D2PhiPi_MS_2018.py
   
8) To be run with Turbo05 data, stream : CHARMSPEC.
    Ds2PhiPi_MS_2018.py


---------- Paths for the options files : ----------


1) paths for D2KS0H_D2PhiPi_Run1.py options file:

 - 2011 Dw
    sim+std://LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20r1/WGCharmSelection2/90000000/CHARM.MDST
 - 2011 Up
    sim+std://LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20r1/WGCharmSelection2/90000000/CHARM.MDST
 - 2012 Dw
    sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping20/WGCharmSelection2/90000000/CHARM.MDST
 - 2012 Up
    sim+std://LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20/WGCharmSelection2/90000000/CHARM.MDST


2) paths for D2KS0H_D2PhiPi_MS_2015.py options file:

 - 2015 Dw
    sim+std://LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo02/RawRemoved/94000000/TURBO.MDST
 - 2015 Up
    sim+std://LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo02/RawRemoved/94000000/TURBO.MDST


3) paths for D2KS0H_D2PhiPi_MS_2016.py options file:

 - 2016 Dw
    sim+std://LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo03a/94000000/CHARMCHARGED.MDST
 - 2016 Up
    sim+std://LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo03a/94000000/CHARMCHARGED.MDST


4) paths for Ds2PhiPi_MS_2016.py options file:

 - 2016 Dw 100%
   sim+std://LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo03a/94000000/CHARMSPECPARKED.MDST
 - 2016 Up 100%
    sim+std://LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo03a/94000000/CHARMSPECPARKED.MDST 


5) paths for D2KS0H_D2PhiPi_MS_2017.py options file:

 - 2017 Dw
    sim+std://LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMCHARGED.MDST
 - 2017 Up
    sim+std://LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMCHARGED.MDST


6) paths for Ds2PhiPi_MS_2017.py options file:

 - 2017 Dw
    sim+std://LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMSPEC.MDST
 - 2017 Up
    sim+std://LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMSPEC.MDST


7) paths for D2KS0H_D2PhiPi_MS_2018.py options file:

 - 2018 Dw
    sim+std://LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo05/94000000/CHARMCHARGED.MDST
 - 2018 Up
    sim+std://LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo05/94000000/CHARMCHARGED.MDST


8) paths for Ds2PhiPi_MS_2018.py options file:

 - 2018 Dw
    sim+std://LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo05/94000000/CHARMSPEC.MDST
 - 2018 Up
    sim+std://LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo05/94000000/CHARMSPEC.MDST


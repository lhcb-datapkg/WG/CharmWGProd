## Technical details

This information should **not** be needed by anyone who wishes to submit a production, [README.md](https://gitlab.cern.ch/lhcb-datapkg/WG/CharmWGProd/blob/master/README.md) should be used instead.

### How the CI works

TESTING WORKFLOW (commits to non-master branches):
 - push commit:
    - prepare tests tag
 - prepare tests tag:
    - run N tests in parallel over 1000 events

SUBMISSION WORKFLOW (commits to master):
 - push or merge commit:
    - prepare master tests tag
 - prepare master tests tag:
    - run N tests in parallel over 1 LFN
    - MANUAL: Create release
 - releases:
    - MANUAL: Check CVMFS for data package, create steps/productions as required


### Preparing an environment for running the CI locally

The current user must have a grid certificate and be able to use SSH authentication with CERN's gitlab instance.

```bash
# Add spaces before setting GITLAB_API_TOKEN token to avoid adding it to the shell history
  export GITLAB_API_TOKEN="XXXXXXXXXX"
export CI_COMMIT_REF_NAME=""
export CI_PROJECT_DIR="${PWD}"
export CI_PROJECT_PATH="lhcb-datapkg/WG/CharmWGProd"
# export CI_CHARMWGPROD_IS_DEBUG=1
source /cvmfs/lhcb.cern.ch/group_login.sh
lhcb-proxy-init
```

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json
from os.path import abspath, dirname, isdir, isfile, join
import pkgutil

from .externals.jsonschema import Draft4Validator, validators


__all__ = [
    'load_info'
]


def extend_with_default(validator_class):
    validate_properties = validator_class.VALIDATORS["properties"]

    def set_defaults(validator, properties, instance, schema):
        for property, subschema in properties.items():
            if "default" in subschema:
                instance.setdefault(property, subschema["default"])

        for error in validate_properties(validator, properties, instance, schema):
            yield error

    return validators.extend(
        validator_class, {"properties": set_defaults},
    )


def load_info(fn):
    fn = abspath(fn)
    with open(fn, 'rt') as fp:
        info = json.load(fp)
    validate(info)

    for job_name, job_config in info.items():
        # Ensure all of the requested options files exist
        for options_fn in job_config['options']:
            full_options_fn = join(dirname(fn), options_fn)
            if not isfile(full_options_fn):
                raise ValueError(
                    'Failed to find options file "{}" for "{}", expected path was "{}"'
                    .format(options_fn, job_name, full_options_fn)
                )

        # Ensure the requested application exists
        application_path = '/cvmfs/lhcb.cern.ch/lib/lhcb/{0}/{0}_{1}/'.format(
            job_config['application'].upper(), job_config['application_version']
        )
        assert isdir(application_path), (job_name, application_path)

    return info


SchemaValidator = extend_with_default(Draft4Validator)
schema = pkgutil.get_data('wg_prod_ci_tools', 'assets/schema.json')
schema = json.loads(schema.decode('utf-8'))
validate = SchemaValidator(schema).validate

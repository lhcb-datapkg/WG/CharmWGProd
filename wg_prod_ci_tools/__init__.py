from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import distutils.spawn
from subprocess import check_output, CalledProcessError

from .schema import load_info

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError


__all__ = [
    'check_proxy',
    'load_info',
]


def check_proxy():
    try:
        if distutils.spawn.find_executable("dirac-proxy-info"):
            check_output(["dirac-proxy-info"])
        else:
            check_output("lb-run -c 'best' LHCbDIRAC/$LHCBDIRAC_VERSION dirac-proxy-info", shell=True)
    except CalledProcessError as e:
        raise RuntimeError('Unable to find valid grid proxy', e)


check_proxy()

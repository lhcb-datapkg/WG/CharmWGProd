from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
from os.path import dirname, join, isdir


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')
    subparsers.required = True

    validate_parser = subparsers.add_parser(
        'validate',
        help='Validate a info.json file')
    validate_parser.add_argument('production_name', choices=_get_production_names())
    validate_parser.set_defaults(func=validate)

    test_parser = subparsers.add_parser(
        'prepare-ci-tests',
        help='Edit .gitlab-ci.yml to tst a info.json file')
    test_parser.add_argument('production_name', choices=_get_production_names())
    test_parser.set_defaults(func=prepare_ci_tests)

    count_parser = subparsers.add_parser(
        'count-jobs',
        help='Print the number of jobs listen in a given info.json file')
    count_parser.add_argument('production_name', choices=_get_production_names())
    count_parser.set_defaults(func=count_jobs)

    changed_parser = subparsers.add_parser(
        'changed-productions',
        help='List the productions that changed in the last commit')
    changed_parser.set_defaults(func=changed_productions)

    args = parser.parse_args()
    args.func(args)


def _get_filename(args):
    return join(dirname(dirname(__file__)), 'productions', args.production_name, 'info.json')


def _get_production_names():
    return [dname for dname in os.listdir(join(dirname(dirname(__file__)), 'productions'))
            if isdir(join(dirname(dirname(__file__)), 'productions', dname))]


def validate(args):
    import wg_prod_ci_tools
    filename = _get_filename(args)
    wg_prod_ci_tools.load_info(filename)
    print('Successfully validated', filename)


def prepare_ci_tests(args):
    import wg_prod_ci_tools.ci
    filename = _get_filename(args)
    wg_prod_ci_tools.ci.prepare_ci_tests(filename)


def count_jobs(args):
    import wg_prod_ci_tools
    filename = _get_filename(args)
    info = wg_prod_ci_tools.load_info(filename)
    print(len(info))


def changed_productions(args):
    import wg_prod_ci_tools.ci
    print('\n'.join(wg_prod_ci_tools.ci.get_changed_productions()))


if __name__ == '__main__':
    parse_args()

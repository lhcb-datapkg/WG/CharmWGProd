from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from os.path import dirname, isdir, join
import sys

import git

from .schema import load_info


__all__ = [
    'get_changed_productions',
]


def get_changed_productions():
    repo = git.Repo('.')

    if os.environ['CI_COMMIT_REF_NAME'] == 'master':
        diffs = repo.commit('HEAD').diff('HEAD~1')
    else:
        common_ancestor = repo.merge_base('HEAD', 'origin/master')
        diffs = repo.commit('HEAD').diff(common_ancestor)

    production_names = set()
    for diff in diffs:
        path = diff.b_path
        if path is not None and path.startswith('productions/'):
            production_name = path.split(os.sep)[1]
            info_fn = join(dirname(dirname(__file__)), 'productions', production_name, 'info.json')
            if not isdir(dirname(info_fn)):
                print('Skipping', production_name, 'appears to have been removed', file=sys.stderr)
                continue
            if production_name not in production_names and load_info(info_fn):
                production_names.add(production_name)

    return sorted(production_names)
